import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回硬件录入信息 */
export interface EquipmentApiResult extends BaseResult {
  /** 硬件名称 */
  name?: string
  /** 数量 */
  number?: number
  /** 客户id */
  customerId?: string
  /** 客户姓名 */
  customerName?: string
  /** 供应商id */
  supplierId?: string
  /** 供应商名称 */
  supplierName?: string
  /** 客户手机号 */
  phone?: string
  /** 收货地址 */
  address?: string
  /** 下单时间 */
  orderDate?: string
  /** 技术负责人 */
  technologyChargePerson?: string
  /** 采购负责人 */
  purchaseChargePerson?: string
  /** 审核人 */
  examinePerson?: string
  /** 订单类型 */
  type?: string
  /** 快递公司编码 */
  courierCompany?: string
  /** 快递公司名称 */
  courierCompanyName?: string
  /** 快递单号 */
  courierNumber?: string
  /** 备注 */
  remark?: string
  /** 审核备注 */
  auditRemark?: string
  /** 所属状态: 1-审核通过，0-审核不通过，2审核中 */
  status?: 0 | 1 | 2 | number
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateEquipment {
  id?: string
  /** 硬件名称 */
  name?: string
  /** 数量 */
  number?: number
  /** 客户id */
  customerId?: string
  /** 客户姓名 */
  customerName?: string
  /** 供应商id */
  supplierId?: string
  /** 供应商名称 */
  supplierName?: string
  /** 客户手机号 */
  phone?: string
  /** 收货地址 */
  address?: string
  /** 下单时间 */
  orderDate?: string
  /** 技术负责人 */
  technologyChargePerson?: string
  /** 采购负责人 */
  purchaseChargePerson?: string
  /** 审核人 */
  examinePerson?: string
  /** 订单类型 */
  type?: string
  /** 快递公司编码 */
  courierCompany?: string
  /** 快递公司名称 */
  courierCompanyName?: string
  /** 快递单号 */
  courierNumber?: string
  /** 备注 */
  remark?: string
  /** 审核备注 */
  auditRemark?: string
  /** 所属状态: 1-审核通过，0-审核不通过，2审核中 */
  status?: 0 | 1 | 2 | number
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
  [key: string]: string | number | undefined | 0 | 1 | Array<string>
}

export interface QueryEquipmentList extends Pagination {
  /** 硬件名称 */
  name?: string
  /** 客户名称 */
  customerId?: string
  customerName?: string
  /** 供应商名称 */
  supplierName?: string
  /** 技术负责人 */
  technologyChargePerson?: string
  /** 采购负责人 */
  purchaseChargePerson?: string
  /** 审核负责人 */
  examinePerson?: string
  /** 客户手机号 */
  phone?: string
  /** 订单类型 */
  type?: string
  typeText?: string
  /** 快递公司 */
  courierCompany?: string
  /** 快递公司名称 */
  courierCompanyName?: string
  /** 快递单号 */
  courierNumber?: string
  /** 下单日期 */
  orderDate?: string
}

// 获取硬件录入单条信息
export function getEquipmentInfo(id?: string): Promise<ResultData<EquipmentApiResult>> {
  return request<ResultData<EquipmentApiResult>>({
    url: '/equipment/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取硬件录入分页查询
export function getEquipmentList(params: QueryEquipmentList): Promise<ResultData<ListResultData<EquipmentApiResult>>> {
  return request<ResultData<ListResultData<EquipmentApiResult>>>({
    url: '/equipment/list',
    method: ApiMethodContants.GET,
    params
  })
}

// 更新硬件录入
export function updateEquipment(data: ICreateOrUpdateEquipment): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/equipment',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建硬件录入
export function createEquipment(data: ICreateOrUpdateEquipment): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/equipment',
    method: ApiMethodContants.POST,
    data
  })
}

// 删除硬件录入
export function deleteEquipment(data: ICreateOrUpdateEquipment): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/equipment/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

// 硬件录入状态
export function updateStatus(data: EquipmentApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/equipment/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

// 获取硬件录入快递信息
export function getEquipmentExpress(id?: string): Promise<any> {
  return request({
    url: '/equipment/express',
    method: ApiMethodContants.GET,
    params: { id }
  })
}
