import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回留言 */
export interface MessageApiResult extends BaseResult {
  /** 姓名 */
  name?: string
  /** 手机号 */
  phone?: string
  /** 邮箱 */
  email?: string
  /** 留言ip */
  messageIp?: string
  /** 留言地区 */
  address?: string
  /** 留言来源 */
  messageUrl?: string
  /** 留言内容名称 */
  contentName?: string
  /** 留言内容Id */
  contentId?: string
  /** 留言需求 */
  content?: string
  /** 是否已读 1-已读 0-未读 */
  status?: 0 | 1 | string
  /** 是否删除 */
  isDelete?: 0 | 1 | string
}


export interface ICreateOrUpdateMessage {
  id?: string
  name?: string
  phone?: string
  email?: string
  messageIp?: string
  address?: string
  messageUrl?: string
  contentName?: string
  contentId?: string
  content?: string
  status?: 0 | 1 | string | undefined
}

export interface QueryMessageList extends Pagination {
  /** 品牌名称 */
  name?: string
  /** 手机号 */
  phone?: string
  /** 微信号 */
  email?: string
  /** 留言需求 */
  content?: string
  /** 是否已读 1-已读 0-未读 */
  status?: 0 | 1 | string
}

// 获取留言单条信息
export function getMessageInfo(id?: string): Promise<ResultData<MessageApiResult>> {
  return request<ResultData<MessageApiResult>>({
    url: '/message/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取客户留言分页查询
export function getMessageList(params: QueryMessageList): Promise<ResultData<ListResultData<MessageApiResult>>> {
  return request<ResultData<ListResultData<MessageApiResult>>>({
    url: '/message/list',
    method: ApiMethodContants.GET,
    params
  })
}

// 更新留言状态
export function updateStatus(data: MessageApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/message/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

// 删除留言
export function deleteMessage(data: MessageApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/message/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

