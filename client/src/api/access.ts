import request from '@/utils/request'
import config from '@/config/index'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'
import { getRefreshToken } from '@/utils/cache'

/** 返回访问统计 */
export interface AccessApiResult extends BaseResult {
  /** 访问统计用户的姓名 */
  userName?: string
  /** 游客姓名 */
  userEmail?: string
  /** 访问页面 */
  accessPage?: string
  /** 来源网站 */
  sourceSite?: string
  /** 地区 */
  district?: string
  /** 访问时间 */
  accessDate?: string
  /** 访问设备 */
  accessEquipment?: string
  /** 用户类型 0-超管 1-普通用户 */
  type?: 0 | 1
  /** 用户状态 1-活动中 0-禁用 */
  status?: 0 | 1 | string
}

export interface ICreateOrUpdateAccess {
  id?: string
  userName?: string
  userEmail?: string
  accessPage?: string
  sourceSite?: string
  district?: string
  accessDate?: string
  accessEquipment?: string
}

export interface QueryAccessList extends Pagination {
  /** 地区 */
  district?: string
}

/** 网站用户详情 */
export function getAccessInfo(id?: string): Promise<ResultData<AccessApiResult>> {
  return request<ResultData<AccessApiResult>>({
    url: '/access/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

/** 访问统计分页查询 */
export function getAccessList(params: QueryAccessList): Promise<ResultData<ListResultData<AccessApiResult>>> {
  return request<ResultData<ListResultData<AccessApiResult>>>({
    url: '/access/list',
    method: ApiMethodContants.GET,
    params
  })
}
