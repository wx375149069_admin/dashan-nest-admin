import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回网站信息 */
export interface InfoApiResult extends BaseResult {
  /** 网站名称 */
  webName?: string
  /** 描述 */
  description?: string
  /** 关键词 */
  keywords?: string
  /** 品牌名称 */
  brandName?: string
  /** 公司名称 */
  companyName?: string
  /** 公众号名称 */
  wechatPublicName?: string
  /** 联系方式 */
  phone?: string
  /** 邮箱 */
  email?: string
  /** 官方微信号 */
  wechat?: string
  /** skype */
  skype?: string
  /** 地址 */
  address?: string,
  /** 版权 */
  copyright?: string,
  /** 网站logo */
  logo?: string
  /** 二维码 */
  qrCode?: string
}

export interface ICreateOrUpdateInfo {
  id?: string,
  webName?: string
  description?: string
  keywords?: string
  brandName?: string
  companyName?: string
  wechatPublicName?: string
  phone?: string,
  email?: string,
  wechat?: string
  skype?: string
  address?: string
  copyright?: string
  logo: string
  qrCode?: string
}

// 获取网站信息
export function getWebInfo(): Promise<ResultData<InfoApiResult>> {
  return request<ResultData<InfoApiResult>>({
    url: '/info',
    method: ApiMethodContants.GET
  })
}

// 更新网站信息
export function updateWebInfo(data: ICreateOrUpdateInfo): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/info',
    method: ApiMethodContants.PUT,
    data
  })
}

