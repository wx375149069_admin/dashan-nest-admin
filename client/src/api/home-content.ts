import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回首页内容 */
export interface HomeContentApiResult extends BaseResult {
  /** 首页内容标题 */
  title?: string
  /** 首页内容描述 */
  description?: string
  /** 首页内容跳转链接 */
  url?: string
  /** 首页内容标识 */
  code?: string
  /** 首页内容图片组合 */
  imgs?: string
  /** 首页内容 1-启用 0-禁用 */
  status?: 0 | 1 | string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateHomeContent {
  id?: string
  title?: string
  description?: string
  url?: string
  code?: string
  imgs?: string
  status?: 0 | 1 | string
  isDelete?: 0 | 1 | string
  [key: string]: string | number | undefined | 0 | 1 | Array<string>
}

export interface QueryHomeContentList extends Pagination {
  /** 首页内容标题 */
  title?: string
  /** 首页内容标识 */
  code?: string
  /** 首页内容 1-启用 0-禁用 */
  status?: 0 | 1 | string,
}

// 获取首页内容单条信息
export function getHomeContentInfo(id?: string): Promise<ResultData<HomeContentApiResult>> {
  return request<ResultData<HomeContentApiResult>>({
    url: '/homeContent/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 根据标识获取首页内容单条信息
export function getHomeContentInfoData(code?: string): Promise<ResultData<HomeContentApiResult>> {
  return request<ResultData<HomeContentApiResult>>({
    url: `/homeContent/info/${code}`,
    method: ApiMethodContants.GET
  })
}

// 获取首页内容分页查询
export function getHomeContentInfoList(params: QueryHomeContentList): Promise<ResultData<ListResultData<HomeContentApiResult>>> {
  return request<ResultData<ListResultData<HomeContentApiResult>>>({
    url: '/homeContent/list',
    method: ApiMethodContants.GET,
    params
  })
}

// 更新首页内容
export function updateHomeContentInfo(data: ICreateOrUpdateHomeContent): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/homeContent',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建首页内容
export function createHomeContentInfo(data: ICreateOrUpdateHomeContent): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/homeContent',
    method: ApiMethodContants.POST,
    data
  })
}

// 更新首页内容状态
export function updateStatus(data: HomeContentApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/homeContent/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

// 删除首页内容
export function deleteHomeContentInfo(data: HomeContentApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/homeContent/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

