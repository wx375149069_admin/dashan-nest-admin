import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回登录日志 */
export interface LoginlogApiResult extends BaseResult {
  /** 用户名称 */
  userName?: string
  /** 登录ip */
  loginIp?: string
  /** 登录地点 */
  loginAddress?: string
  /** 登录浏览器 */
  loginBrowser?: string
  /** 登录系统 */
  loginSystem?: string
  /** 日志状态 1-成功 0-失败 */
  status?: 0 | 1 | string
}

export interface QueryLoginlogList extends Pagination {
  /** 用户名称 */
  userName?: string
  /** 登录ip */
  loginIp?: string
  /** 登录地点 */
  loginAddress?: string
  /** 登录浏览器 */
  loginBrowser?: string
  /** 登录系统 */
  loginSystem?: string
  /** 日志状态 1-成功 0-失败 */
  status?: 0 | 1 | string
}

// 获取登录日志分页查询
export function getLoginlogList(params: QueryLoginlogList): Promise<ResultData<ListResultData<LoginlogApiResult>>> {
  return request<ResultData<ListResultData<LoginlogApiResult>>>({
    url: '/loginlog/list',
    method: ApiMethodContants.GET,
    params
  })
}
