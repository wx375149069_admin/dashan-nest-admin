import request from '@/utils/request'
import config from '@/config/index'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'
import { getRefreshToken } from '@/utils/cache'

/** 返回用户类型 */
export interface UserApiResult extends BaseResult {
  /** 账号 */
  account?: string
  /** 姓名 */
  name?: string
  /** 用户手机号 */
  phoneNum?: string
  /** 性别 */
  sex?: string
  /** 用户邮箱 */
  email?: string
  /** 用户邮箱 */
  address?: string
  /** 用户类型 0-超管 1-普通用户 */
  type?: 0 | 1
  /** 用户状态 1-活动中 0-禁用 */
  status?: 0 | 1 | string
}

export interface ICreateOrUpdateUser {
  id?: string
  account?: string
  name?: string
  phoneNum?: string
  sex?: string
  email?: string
  address?: string
  status?: 0 | 1 | string
  password?: string
}

export interface ICreateSendMail {
  receiveEmail?: string
  emailMarketing?: string
  content?: string
}

export interface QueryUserList extends Pagination {
  /** 帐号，手机号，名称 */
  account?: string
  /** 姓名 */
  email?: string
  /** 姓名 */
  name?: string
  /** 地址 */
  address?: string
  /** 用户是否可用 */
  status?: string | 0 | 1
}

/** 网站用户详情 */
export function getUserInfo(id?: string): Promise<ResultData<UserApiResult>> {
  return request<ResultData<UserApiResult>>({
    url: '/clientuser/web/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

/** 网站用户分页查询 */
export function getUserList(params: QueryUserList): Promise<ResultData<ListResultData<UserApiResult>>> {
  return request<ResultData<ListResultData<UserApiResult>>>({
    url: '/clientuser/list',
    method: ApiMethodContants.GET,
    params
  })
}

/** 网站用户更新状态 */
export function updateStatus(data: ICreateOrUpdateUser): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/clientuser/status',
    method: ApiMethodContants.PUT,
    data
  })
}

/** 发送邮件 */
export function sendMail(data: ICreateSendMail): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/clientuser/sendMail',
    method: ApiMethodContants.POST,
    data
  })
}

