import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回系统设置 */
export interface SettingApiResult extends BaseResult {
  /** 邮箱授权码 */
  emailCode?: string
  /** 发送邮件的邮箱 */
  emailMail?: string
  /** 接收邮件的邮箱 */
  receiveMail?: string
  /** 发送邮件的端口 */
  emailPort?: number
  /** 邮件营销语 */
  emailMarketing?: string
  /** 短信AccessKey */
  smsAccessKey?: string
  /** 短信Access密钥 */
  smsAccessSecret?: string
  /** 短信签名 */
  smsSignName?: string
  /** 短信签名 */
  smsCode?: string
  /** OSS的AccessKey */
  ossAccessKey?: string
  /** OSS的Access密钥 */
  ossAccessSecret?: string
  /** OSS的region */
  ossRegion?: string
  /**  OSS的bucket */
  ossBucket?: string
  /**  快递鸟用户id */
  logisticsUserId?: string
  /**  快递鸟API的key */
  logisticsKey?: string
}

export interface ICreateOrUpdateSetting {
  id?: string
  emailCode?: string
  emailMail?: string
  receiveMail?: string
  emailPort?: number
  emailMarketing?: string
  smsAccessKey?: string
  smsAccessSecret?: string
  smsSignName?: string
  smsCode?: string
  ossAccessKey?: string
  ossAccessSecret?: string
  ossRegion?: string
  ossBucket?: string
  logisticsUserId?: string
  logisticsKey?: string
}

export interface CreateEmail {
  content?: string
}

export interface CreateSms {
  phone?: number
}

// 获取系统设置
export function getSetting(): Promise<ResultData<SettingApiResult>> {
  return request<ResultData<SettingApiResult>>({
    url: '/setting',
    method: ApiMethodContants.GET
  })
}

// 更新系统设置
export function updateSetting(data: ICreateOrUpdateSetting): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/setting',
    method: ApiMethodContants.PUT,
    data
  })
}


// 测试发送邮箱
export function sendMailApi(data: CreateEmail): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/setting/sendMail',
    method: ApiMethodContants.POST,
    data
  })
}

// 测试发送短信
export function sendSmsApi(data: CreateSms): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/setting/sendSms',
    method: ApiMethodContants.POST,
    data
  })
}

// 测试查询物流信息
export function sendLogistics(): Promise<any> {
  return request({
    url: '/setting/sendLogistics',
    method: ApiMethodContants.GET,
  })
}
