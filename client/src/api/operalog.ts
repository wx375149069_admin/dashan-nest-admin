import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回操作日志 */
export interface OperalogApiResult extends BaseResult {
  /** 系统菜单 */
  systemMenu?: string
  /** 操作模块 */
  operaModule?: string
  /** 操作人员 */
  operaName?: string
  /** 操作IP */
  operaIp?: string
  /** 日志状态 1-成功 0-失败 */
  status?: 0 | 1 | string
}

export interface QueryOperalogList extends Pagination {
  /** 系统菜单 */
  systemMenu?: string
  /** 操作模块 */
  operaModule?: string
  /** 操作人员 */
  operaName?: string
  /** 操作IP */
  operaIp?: string
  /** 日志状态 1-成功 0-失败 */
  status?: 0 | 1 | string
}

// 获取操作日志分页查询
export function getOperalogList(params: QueryOperalogList): Promise<ResultData<ListResultData<OperalogApiResult>>> {
  return request<ResultData<ListResultData<OperalogApiResult>>>({
    url: '/operalog/list',
    method: ApiMethodContants.GET,
    params
  })
}
