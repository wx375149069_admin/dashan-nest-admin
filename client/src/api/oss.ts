import request from '@/utils/request'
import { type BaseResult, type Pagination, type ResultData, ApiMethodContants } from './base'

export interface OssApiResult extends BaseResult {
  // 预览路径
  urlAll: string
  // 文件路径
  url: string
  // 文件size
  size: number
  // 文件 mimetype
  type: string
  // 注释
  business: string

  userId: string

  userAccount: string

  createDate: string
  // oss文件是否选中
  checked: boolean | undefined
  // oss文件类型
  fileType: string | undefined
}

export interface uploadFile extends BaseResult {
  // 文件路径
  url: string
  // 名称
  name: string | undefined
  // uid
  uid: number
  // 状态
  status: string | undefined
}

export interface findOssList extends Pagination {
  startDay?: string

  endDay?: string
}

export function getFileList(params: findOssList): Promise<ResultData<OssApiResult>> {
  return request({
    url: '/oss/list',
    method: ApiMethodContants.GET,
    params
  })
}

export function fileUpload(data: FormData): Promise<ResultData<OssApiResult[]>> {
  return request({
    url: '/oss/upload',
    method: ApiMethodContants.POST,
    data
  })
}
