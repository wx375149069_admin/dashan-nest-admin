import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回项目信息 */
export interface ProjectMonitoringApiResult extends BaseResult {
  /** 项目名称 */
  projectName?: string
  /** 项目Key */
  projectKey?: string
  /** 负责人姓名 */
  userName?: string
  /** 项目 1-启用 0-禁用 */
  status?: 0 | 1 | string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateProjectMonitoring {
  id?: string
  projectName?: string
  projectKey?: string
  userName?: string
  status?: 0 | 1 | string
  isDelete?: 0 | 1 | string
}

export interface QueryProjectMonitoringList extends Pagination {
  /** 项目名称 */
  projectName?: string
  /** 负责人姓名 */
  userName?: string
  /** 项目 1-启用 0-禁用 */
  status?: 0 | 1 | string,
}

// 获取项目单条信息
export function getProjectMonitoringInfo(id?: string): Promise<ResultData<ProjectMonitoringApiResult>> {
  return request<ResultData<ProjectMonitoringApiResult>>({
    url: '/projectMonitoring/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取项目分页查询
export function getProjectMonitoringList(params: QueryProjectMonitoringList): Promise<ResultData<ListResultData<ProjectMonitoringApiResult>>> {
  return request<ResultData<ListResultData<ProjectMonitoringApiResult>>>({
    url: '/projectMonitoring/list',
    method: ApiMethodContants.GET,
    params
  })
}

// 更新项目
export function updateProjectMonitoring(data: ICreateOrUpdateProjectMonitoring): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/projectMonitoring',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建项目
export function createProjectMonitoring(data: ICreateOrUpdateProjectMonitoring): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/projectMonitoring',
    method: ApiMethodContants.POST,
    data
  })
}

// 更新项目状态
export function updateStatus(data: ProjectMonitoringApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/projectMonitoring/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

// 删除项目
export function deleteProjectMonitoring(data: ProjectMonitoringApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/projectMonitoring/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}


/** 返回项目信息 */
export interface monitoringLogApiResult extends BaseResult {
  /** 项目Id */
  projectId: string
  /** 项目名称 */
  projectName: string
  /** 页面url */
  pageUrl: string
  /** 接口地址 */
  apiUrl: string
  /** 接口请求方式body/get */
  apiMethod: string
  /** 接口请求参数 */
  apiParams: string
  /** 接口header参数 */
  apiHeaderParams: string
  /** 后端返回的信息 */
  apiInfo: string
  /** 用户id */
  userId: string
  /** 用户姓名/昵称 */
  userName: string
}

export interface ICreateOrUpdateMonitoringLog {
  id?: string
  projectId?: string
  projectName?: string
  pageUrl?: string
  apiUrl?: string
  apiMethod?: string
  apiParams: string
  apiHeaderParams: string
  apiInfo: string
  userId?: string
  userName?: string
}

export interface QueryMonitoringLogList extends Pagination {
  /** 项目名称 */
  projectName?: string
  /** 创建时间 */
  createDate?: string
}

// 获取监控单条信息
export function getMonitoringLogInfo(id?: string): Promise<ResultData<monitoringLogApiResult>> {
  return request<ResultData<monitoringLogApiResult>>({
    url: '/projectMonitoring/logOne',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取项目分页查询
export function getMonitoringLogList(params: QueryMonitoringLogList): Promise<ResultData<ListResultData<monitoringLogApiResult>>> {
  return request<ResultData<ListResultData<monitoringLogApiResult>>>({
    url: '/projectMonitoring/log/list',
    method: ApiMethodContants.GET,
    params
  })
}
