import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

export interface QueryClientUser {
  list?: Array<object>,
  total?: number
}

/** 查询客户端用户列表所有 */
export function getClientUser(): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/public/clientUserList',
    method: ApiMethodContants.GET
  })
}

export interface QueryMessage {
  isNoReadNumber?: number,
  isReadNumber?: number,
  allNumber?: number
}

/** 查询留言所有 */
export function getMessage(): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/public/messageNumber',
    method: ApiMethodContants.GET
  })
}

export interface QueryAccess {
  yearMonth?: Array<string>
  dataCount?: Array<number>
}

/** 获取过去12个月的访问数据 */
export function getFormerlyMonth(): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/public/getAccess',
    method: ApiMethodContants.GET
  })
}

export interface GetAccess {
  consultNum?: number
  collectNum?: number
  accessNum?: number
}

export function getAccessStatis(): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/public/access/statis',
    method: ApiMethodContants.GET
  })
}

export interface getVersion {
  author?: string
  name?: string
  version?: string
}

export function getVersionUpdate(): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: `${location.origin}/public/version.json`,
    method: ApiMethodContants.GET
  })
}
