import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回轮播图 */
export interface BannerApiResult extends BaseResult {
  /** 轮播图名称 */
  title?: string
  /** 轮播图描述 */
  description?: string
  /** 轮播图跳转链接 */
  url?: string
  /** 轮播图pc端图片 */
  img?: string
  /** 轮播图pc端图片 */
  wapImg?: string
  /** 轮播图 1-启用 0-禁用 */
  status?: 0 | 1 | string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateBanner {
  id?: string
  title?: string
  description?: string
  url?: string
  img?: string
  wapImg?: string
  status?: 0 | 1 | string
  isDelete?: 0 | 1 | string
  [key: string]: string | number | undefined | 0 | 1 | Array<string>
}

export interface QueryBannerList extends Pagination {
  /** 轮播图名称 */
  title?: string
  /** 轮播图跳转链接 */
  url?: string
  /** 轮播图pc端图片 */
  img?: string
  /** 轮播图移动端端图片 */
  wapImg?: string
  /** 轮播图 1-启用 0-禁用 */
  status?: 0 | 1 | string,
}

// 获取轮播图单条信息
export function getBannerInfo(id?: string): Promise<ResultData<BannerApiResult>> {
  return request<ResultData<BannerApiResult>>({
    url: '/banner/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取轮播图分页查询
export function getBannerList(params: QueryBannerList): Promise<ResultData<ListResultData<BannerApiResult>>> {
  return request<ResultData<ListResultData<BannerApiResult>>>({
    url: '/banner/list',
    method: ApiMethodContants.GET,
    params
  })
}

// 获取轮播图分页查询
export function getBannerAllList(): Promise<ResultData<ListResultData<BannerApiResult>>> {
  return request<ResultData<ListResultData<BannerApiResult>>>({
    url: '/banner/list/web',
    method: ApiMethodContants.GET
  })
}

// 更新轮播图
export function updateBanner(data: ICreateOrUpdateBanner): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/banner',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建轮播图
export function createBanner(data: ICreateOrUpdateBanner): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/banner',
    method: ApiMethodContants.POST,
    data
  })
}

// 更新轮播图状态
export function updateStatus(data: BannerApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/banner/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

// 删除轮播图
export function deleteBanner(data: BannerApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/banner/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

