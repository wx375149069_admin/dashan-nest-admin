import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回客户 */
export interface CustomerApiResult extends BaseResult {
  /** 客户姓名 */
  name?: string
  /** 客户手机号 */
  phone?: string
  /** 客户邮箱 */
  email?: string
  /** 客户地址 */
  address?: string
  /** 备注 */
  remark?: string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateCustomer {
  id?: string
  name?: string
  phone?: string
  email?: string
  address?: string
  remark?: string
  isDelete?: 0 | 1 | string
}

export interface QueryCustomerList extends Pagination {
  /** 客户姓名 */
  name?: string
  /** 客户手机号 */
  phone?: string
}

// 获取客户单条信息
export function getCustomerInfo(id?: string): Promise<ResultData<CustomerApiResult>> {
  return request<ResultData<CustomerApiResult>>({
    url: '/customer/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取客户分页查询
export function getCustomerList(params: QueryCustomerList): Promise<ResultData<ListResultData<CustomerApiResult>>> {
  return request<ResultData<ListResultData<CustomerApiResult>>>({
    url: '/customer/list',
    method: ApiMethodContants.GET,
    params
  })
}

// 更新客户
export function updateCustomer(data: ICreateOrUpdateCustomer): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/customer',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建客户
export function createCustomer(data: ICreateOrUpdateCustomer): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/customer',
    method: ApiMethodContants.POST,
    data
  })
}

// 删除客户
export function deleteCustomer(data: CustomerApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/customer/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

/** 返回供应商 */
export interface SupplierApiResult extends BaseResult {
  /** 供应商名称 */
  name?: string
  /** 供应商联系人 */
  userName?: string
  /** 供应商手机号 */
  phone?: string
  /** 供应商邮箱 */
  email?: string
  /** 供应商地址 */
  address?: string
  /** 备注 */
  remark?: string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateSupplier {
  id?: string
  name?: string
  userName?: string
  phone?: string
  email?: string
  address?: string
  remark?: string
  isDelete?: 0 | 1 | string
}

export interface QuerySupplierList extends Pagination {
  /** 供应商名称 */
  name?: string
  /** 供应商联系人 */
  userName?: string
  /** 供应商手机号 */
  phone?: string
}

// 获取客户单条信息
export function getSupplierInfo(id?: string): Promise<ResultData<SupplierApiResult>> {
  return request<ResultData<SupplierApiResult>>({
    url: '/supplier/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取客户分页查询
export function getSupplierList(params: QuerySupplierList): Promise<ResultData<ListResultData<SupplierApiResult>>> {
  return request<ResultData<ListResultData<SupplierApiResult>>>({
    url: '/supplier/list',
    method: ApiMethodContants.GET,
    params
  })
}

// 更新客户
export function updateSupplier(data: ICreateOrUpdateSupplier): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/supplier',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建客户
export function createSupplier(data: ICreateOrUpdateSupplier): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/supplier',
    method: ApiMethodContants.POST,
    data
  })
}

// 删除客户
export function deleteSupplier(data: SupplierApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/supplier/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

