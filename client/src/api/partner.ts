import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回合作伙伴 */
export interface PartnerApiResult extends BaseResult {
  /** 合作伙伴名称 */
  partnerName?: string
  /** 合作伙伴手机号 */
  partnerPhone?: string
  /** 合作伙伴联系人 */
  partnerUserName?: string
  /** 合作伙伴logo */
  logo?: string
  /** 合作伙伴 1-启用 0-禁用 */
  status?: 0 | 1 | string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdatePanter {
  id?: string
  partnerName?: string
  partnerPhone?: string
  partnerUserName?: string
  logo?: string
  status?: 0 | 1 | string
  isDelete?: 0 | 1 | string
}

export interface QueryPartnerList extends Pagination {
  /** 合作伙伴logo */
  logo?: string
  /** 合作伙伴名称 */
  partnerName?: string
  /** 合作伙伴手机号 */
  partnerPhone?: string
  /** 合作伙伴联系人 */
  partnerUserName?: string
  /** 合作伙伴 1-启用 0-禁用 */
  status?: 0 | 1 | string,
}

// 获取合作伙伴单条信息
export function getPartnerInfo(id?: string): Promise<ResultData<PartnerApiResult>> {
  return request<ResultData<PartnerApiResult>>({
    url: '/partner/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取合作伙伴分页查询
export function getPartnerList(params: QueryPartnerList): Promise<ResultData<ListResultData<PartnerApiResult>>> {
  return request<ResultData<ListResultData<PartnerApiResult>>>({
    url: '/partner/list',
    method: ApiMethodContants.GET,
    params
  })
}

// 更新合作伙伴
export function updatePartner(data: ICreateOrUpdatePanter): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/partner',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建合作伙伴
export function createPartner(data: ICreateOrUpdatePanter): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/partner',
    method: ApiMethodContants.POST,
    data
  })
}

// 更新合作伙伴状态
export function updateStatus(data: PartnerApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/partner/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

// 删除合作伙伴
export function deletePartner(data: PartnerApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/partner/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

