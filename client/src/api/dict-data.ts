import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回字典 */
export interface DictDataApiResult extends BaseResult {
  /** 字典名称 */
  dictLabel?: string
  /** 字典键值 */
  dictValue?: string
  /** 字典描述 */
  description?: string
  /** 字典内容 */
  content?: string
  /** 字典id */
  dictId?: string
  /** 字典类型 */
  dictType?: string
  /** 字典 1-启用 0-禁用 */
  status?: 0 | 1 | string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateDictData {
  id?: string
  dictLabel?: string
  dictValue?: string
  description?: string
  content?: string
  dictId?: string,
  dictType?: string
  status?: 0 | 1 | string
  isDelete?: 0 | 1 | string
}

export interface QueryDictDataList extends Pagination {
  /** 字典名称 */
  dictLabel?: string
  /** 字典键值 */
  dictValue?: string
  /** 字典类型 */
  dictType?: string
  /** 字典 1-启用 0-禁用 */
  status?: 0 | 1 | string,
}

// 获取字典单条信息
export function getDictInfo(id?: string): Promise<ResultData<DictDataApiResult>> {
  return request<ResultData<DictDataApiResult>>({
    url: '/dict/data/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取字典分页查询
export function getDictList(params: QueryDictDataList): Promise<ResultData<ListResultData<DictDataApiResult>>> {
  return request<ResultData<ListResultData<DictDataApiResult>>>({
    url: '/dict/list/data',
    method: ApiMethodContants.GET,
    params
  })
}

// 更新字典
export function updateDict(data: ICreateOrUpdateDictData): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/dict/data',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建字典
export function createDict(data: ICreateOrUpdateDictData): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/dict/data',
    method: ApiMethodContants.POST,
    data
  })
}

// 更新字典状态
export function updateStatus(data: DictDataApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/dict/data/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

// 删除字典
export function deleteDict(data: DictDataApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/dict/data/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

