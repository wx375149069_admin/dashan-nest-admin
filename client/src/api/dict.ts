import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回字典 */
export interface DictApiResult extends BaseResult {
  /** 字典名称 */
  dictName?: string
  /** 字典类型 */
  dictType?: string
  /** 字典描述 */
  description?: string
  /** 字典内容 */
  content?: string
  /** 字典 1-启用 0-禁用 */
  status?: 0 | 1 | string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateDict {
  id?: string
  dictName?: string
  dictType?: string
  description?: string
  content?: string
  status?: 0 | 1 | string
  isDelete?: 0 | 1 | string
}

export interface QueryDictList extends Pagination {
  /** 字典名称 */
  dictName?: string
  /** 字典手机号 */
  dictType?: string
  /** 字典 1-启用 0-禁用 */
  status?: 0 | 1 | string,
}

// 获取字典单条信息
export function getDictInfo(id?: string): Promise<ResultData<DictApiResult>> {
  return request<ResultData<DictApiResult>>({
    url: '/dict/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 根据类型获取字典单条信息
export function getDictData(dictType?: string): Promise<ResultData<DictApiResult>> {
  return request<ResultData<DictApiResult>>({
    url: `/dict/info/${dictType}`,
    method: ApiMethodContants.GET
  })
}

// 获取字典分页查询
export function getDictList(params: QueryDictList): Promise<ResultData<ListResultData<DictApiResult>>> {
  return request<ResultData<ListResultData<DictApiResult>>>({
    url: '/dict/list',
    method: ApiMethodContants.GET,
    params
  })
}

// 更新字典
export function updateDict(data: ICreateOrUpdateDict): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/dict',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建字典
export function createDict(data: ICreateOrUpdateDict): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/dict',
    method: ApiMethodContants.POST,
    data
  })
}

// 更新字典状态
export function updateStatus(data: DictApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/dict/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

// 删除字典
export function deleteDict(data: DictApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/dict/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

