import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回内容 */
export interface ContentApiResult extends BaseResult {
  /** 内容标题 */
  title?: string
  /** 内容简介 */
  intro?: string
  /** 内容栏目 */
  columnId?: string
  /** 内容标签 */
  tags?: Array<string>
  /** 内容推荐/头条 */
  recom?: string
  /** 内容发布时间 */
  publishDate?: string
  /** 内容价格 */
  price?: number
  /** 内容原价 */
  originalPrice?: number
  /** 内容内容封面图片组 */
  img?: string
  /** 内容seo关键词 */
  keyword?: string
  /** 内容seo描述 */
  description?: string
  /** 内容详情 */
  content?: string | undefined
  /** 内容排序 */
  orderNum?: number
  /** 收藏量  */
  collectNum?: number
  /** 咨询量 */
  consultNum?: number
  /** 访问量  */
  readNum?: number
  /** 内容 1-启用 0-禁用 */
  status?: 0 | 1 | string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateContent {
  id?: string
  title?: string
  intro?: string
  columnId?: string
  tags?: Array<string>
  recom?: string
  publishDate?: string
  price?: number
  originalPrice?: number
  img?: string
  keyword?: string
  description?: string
  content?: string | undefined
  orderNum?: number
  collectNum?: number
  consultNum?: number
  readNum?: number
  status?: 0 | 1 | string
  isDelete?: 0 | 1 | string
  [key: string]: string | number | undefined | 0 | 1 | Array<string>
}

export interface QueryContentList extends Pagination {
  /** 内容标题 */
  title?: string
  /** 内容推荐 */
  recomText?: string
  /** 内容标签 */
  tagsText?: string
  /** 内容栏目 */
  columnId?: string
  /** 内容排序 */
  orderNum?: number
  /** 开始日期 */
  startDate?: string
  /** 结束日期 */
  endDate?: string
  /** 内容 1-启用 0-禁用 */
  status?: 0 | 1 | string
}

// 获取内容单条信息
export function getContentInfo(id?: string): Promise<ResultData<ContentApiResult>> {
  return request<ResultData<ContentApiResult>>({
    url: '/content/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取内容分页查询
export function getContentList(params: QueryContentList): Promise<ResultData<ListResultData<ContentApiResult>>> {
  return request<ResultData<ListResultData<ContentApiResult>>>({
    url: '/content/list',
    method: ApiMethodContants.GET,
    params
  })
}


// 更新内容
export function updateContent(data: ICreateOrUpdateContent): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建内容
export function createContent(data: ICreateOrUpdateContent): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content',
    method: ApiMethodContants.POST,
    data
  })
}

// 删除内容
export function deleteContent(data: ContentApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

// 更新内容状态
export function updateStatus(data: ContentApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

