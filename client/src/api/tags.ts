import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回标签 */
export interface TagsApiResult extends BaseResult {
  /** 标签名称 */
  title?: string
  /** 标签描述 */
  orderNum?: number
  /** 标签 1-启用 0-禁用 */
  status?: 0 | 1 | string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateTags {
  id?: string
  title?: string
  orderNum?: number
  status?: 0 | 1 | string
  isDelete?: 0 | 1 | string
}

export interface AiTags {
  content?: string | undefined
}

export interface QueryTagsList extends Pagination {
  /** 标签名称 */
  title?: string
  /** 标签描述 */
  orderNum?: number
  /** 标签 1-启用 0-禁用 */
  status?: 0 | 1 | string
}

// 获取标签单条信息
export function getTagsInfo(id?: string): Promise<ResultData<TagsApiResult>> {
  return request<ResultData<TagsApiResult>>({
    url: '/content/tags/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取标签分页查询
export function getTagsList(params: QueryTagsList): Promise<ResultData<ListResultData<TagsApiResult>>> {
  return request<ResultData<ListResultData<TagsApiResult>>>({
    url: '/content/tags/list',
    method: ApiMethodContants.GET,
    params
  })
}

// 获取全部标签列表
export function getTagsAllList(): Promise<ResultData<TagsApiResult[]>> {
  return request({
    url: '/content/tags/AllList',
    method: ApiMethodContants.GET,
  })
}

// 更新标签
export function updateTags(data: ICreateOrUpdateTags): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content/tags',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建标签
export function createTags(data: ICreateOrUpdateTags): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content/tags',
    method: ApiMethodContants.POST,
    data
  })
}

// 创建Ai标签
export function createAiTags(data: Array<ICreateOrUpdateTags>): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content/aiTags',
    method: ApiMethodContants.POST,
    data
  })
}

// 更新标签状态
export function updateStatus(data: TagsApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content/tags/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

// 删除标签
export function deleteTags(data: TagsApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content/tags/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

