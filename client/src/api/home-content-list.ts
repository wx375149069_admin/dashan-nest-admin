import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回首页内容列表 */
export interface HomeContentListApiResult extends BaseResult {
  /** 首页内容标题 */
  title?: string
  /** 首页内容描述 */
  description?: string
  /** 首页内容跳转链接 */
  url?: string
  /** 首页内容图片组合 */
  imgs?: string
  /** 首页内容id */
  contentId?: string
  /** 首页内容标识 */
  contentCode?: string
  /** 首页内容 1-启用 0-禁用 */
  status?: 0 | 1 | string
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateHomeContentList {
  id?: string
  title?: string
  description?: string
  url?: string
  imgs?: string
  contentId?: string
  contentCode?: string
  status?: 0 | 1 | string
  isDelete?: 0 | 1 | string
  [key: string]: string | number | undefined | 0 | 1 | Array<string>
}

export interface QueryHomeContentList extends Pagination {
  /** 标题 */
  title?: string
  /** 标识码 */
  contentCode?: string
  /** 首页内容 1-启用 0-禁用 */
  status?: 0 | 1 | string,
}

// 获取首页内容单条信息
export function getHomeContentListInfo(id?: string): Promise<ResultData<HomeContentListApiResult>> {
  return request<ResultData<HomeContentListApiResult>>({
    url: '/homeContent/contentList/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取首页内容分页查询
export function getHomeContentListList(params: QueryHomeContentList): Promise<ResultData<ListResultData<HomeContentListApiResult>>> {
  return request<ResultData<ListResultData<HomeContentListApiResult>>>({
    url: '/homeContent/list/contentList',
    method: ApiMethodContants.GET,
    params
  })
}

// 更新首页内容
export function updateHomeContentList(data: ICreateOrUpdateHomeContentList): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/homeContent/contentList',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建首页内容
export function createHomeContentList(data: ICreateOrUpdateHomeContentList): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/homeContent/contentList',
    method: ApiMethodContants.POST,
    data
  })
}

// 更新首页内容状态
export function updateStatus(data: HomeContentListApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/homeContent/contentList/status/change',
    method: ApiMethodContants.PUT,
    data
  })
}

// 删除首页内容
export function deleteHomeContentList(data: HomeContentListApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/homeContent/contentList/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

