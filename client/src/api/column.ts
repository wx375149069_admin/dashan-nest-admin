import request from '@/utils/request'
import { ApiMethodContants, type BaseResult, type ListResultData, type Pagination, type ResultData } from './base'

/** 返回栏目 */
export interface ColumnApiResult extends BaseResult {
  /** 栏目标题 */
  title?: string
  /** 栏目副标题 */
  subhead?: string
  /** '栏目父级id */
  parentId?: string
  /** 栏目类型 */
  type?: string
  /** 栏目模板 */
  template?: string
  /** 栏目banner */
  img?: string
  /** 栏目seo关键词 */
  keyword?: string
  /** 栏目seo描述 */
  description?: string
  /** 栏目内容 */
  content?: string | undefined
  /** 栏目排序 */
  orderNum?: number
  /** 物理删除 1-未删 0-已删 */
  isDelete?: 0 | 1 | string
}

export interface ICreateOrUpdateColumn {
  id?: string
  title?: string
  subhead?: string
  parentId?: string
  type?: string
  template?: string
  img?: string
  keyword?: string
  description?: string
  content?: string | undefined
  orderNum?: number
  isDelete?: 0 | 1 | string
  [key: string]: string | number | undefined | 0 | 1 | Array<string>
}

export interface QueryColumnList extends Pagination {
  /** 栏目标题 */
  title?: string
  /** 栏目副标题 */
  subhead?: string
  /** 栏目类型 */
  type?: string
  /** 栏目排序 */
  orderNum?: number
}

// 获取栏目单条信息
export function getColumnInfo(id?: string): Promise<ResultData<ColumnApiResult>> {
  return request<ResultData<ColumnApiResult>>({
    url: '/content/column/info',
    method: ApiMethodContants.GET,
    params: { id }
  })
}

// 获取栏目分页查询
export function getColumnList(): Promise<ResultData<ColumnApiResult[]>> {
  return request({
    url: '/content/column/list',
    method: ApiMethodContants.GET,
  })
}

// 获取全部栏目列表
export function getColumnAllList(): Promise<ResultData<ColumnApiResult[]>> {
  return request({
    url: '/content/column/AllList',
    method: ApiMethodContants.GET,
  })
}

// 更新栏目
export function updateColumn(data: ICreateOrUpdateColumn): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content/column',
    method: ApiMethodContants.PUT,
    data
  })
}

// 创建栏目
export function createColumn(data: ICreateOrUpdateColumn): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content/column',
    method: ApiMethodContants.POST,
    data
  })
}

// 删除栏目
export function deleteColumn(data: ColumnApiResult): Promise<ResultData<null>> {
  return request<ResultData<null>>({
    url: '/content/column/delete',
    method: ApiMethodContants.DELETE,
    data
  })
}

