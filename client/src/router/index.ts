import { shallowRef } from 'vue'
import { createRouter, createWebHistory, type RouteRecordRaw } from 'vue-router'

import Layout from '../layout/index.vue'

export const constantRoutes: RouteRecordRaw[] = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/index.vue'),
    meta: { title: '登录', hidden: true }
  },
  {
    path: '/:catchAll(.*)',
    component: () => import('@/views/error/404.vue'),
    meta: {
      title: '页面丢失',
      hidden: true
    }
  },
  {
    path: '/error',
    component: () => import('@/views/error/error.vue'),
    meta: {
      title: '服务端错误',
      hidden: true
    }
  },
  {
    path: '/redirect',
    name: 'redirect',
    component: Layout,
    meta: {
      hidden: true
    },
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index.vue')
      }
    ]
  }
]

/** 使用 shallowRef 消除 警告 */
export const asyncRoutes: RouteRecordRaw[] = [
  {
    path: '/',
    component: shallowRef(Layout),
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import('@/views/dashboard/index.vue'),
        meta: { title: '首页', icon: 'home' }
      }
    ]
  },
  {
    path: '/equipmentAdmin',
    component: shallowRef(Layout),
    name: 'equipmentAdmin',
    redirect: '/equipmentAdmin/customer/index',
    meta: { title: '客户硬件管理', icon: 'component' },
    children: [
      {
        path: 'customer/index',
        component: () => import('@/views/equipmentAdmin/customer/index.vue'),
        name: 'equipment_customer',
        meta: { title: '客户管理' }
      },
      {
        path: 'supplier/index',
        component: () => import('@/views/equipmentAdmin/supplier/index.vue'),
        name: 'equipment_supplier',
        meta: { title: '供应商管理' }
      },
      {
        path: 'equipment/index',
        component: () => import('@/views/equipmentAdmin/equipment/index.vue'),
        name: 'equipment_index',
        meta: { title: '硬件录入管理' }
      }
    ]
  },
  {
    path: '/projectMonitoring',
    component: shallowRef(Layout),
    name: 'projectMonitoring',
    redirect: '/projectMonitoring/admin/index',
    meta: { title: '项目监控', icon: 'component' },
    children: [
      {
        path: 'admin/index',
        component: () => import('@/views/projectMonitoring/admin/index.vue'),
        name: 'project_admin',
        meta: { title: '项目管理' }
      },
      {
        path: 'log/index',
        component: () => import('@/views/projectMonitoring/log/index.vue'),
        name: 'project_log',
        meta: { title: '监控日志' }
      }
    ]
  },
  {
    path: '/home',
    component: shallowRef(Layout),
    name: 'home',
    redirect: '/home/banner',
    meta: { title: '首页AI管理', icon: 'ai-home' },
    children: [
      {
        path: 'banner',
        component: () => import('@/views/home/banner/index.vue'),
        name: 'home_banner',
        meta: { title: 'AI轮播图' }
      },
      {
        path: 'content',
        component: () => import('@/views/home/content/index.vue'),
        name: 'home_content',
        meta: { title: '自定义AI内容' }
      },
      {
        path: 'content/list/:contentCode',
        component: () => import('@/views/home/content/list.vue'),
        name: 'home_content_list',
        meta: { title: '自定义AI内容-列表', hidden: true }
      }
    ]
  },
  {
    path: '/content',
    component: shallowRef(Layout),
    name: 'content',
    redirect: '/content/content',
    meta: { title: 'AI内容管理', icon: 'ai' },
    children: [
      {
        path: 'content',
        component: () => import('@/views/content/content/index.vue'),
        name: 'content_content',
        meta: { title: 'AI内容管理' }
      },
      {
        path: 'column',
        component: () => import('@/views/content/column/index.vue'),
        name: 'content_column',
        meta: { title: 'AI栏目管理' }
      },
      {
        path: 'tags',
        component: () => import('@/views/content/tags/index.vue'),
        name: 'content_tags',
        meta: { title: 'AI标签管理' }
      }
    ]
  },
  {
    path: '/client',
    component: shallowRef(Layout),
    name: 'client',
    redirect: '/client/message',
    meta: { title: '客户端数据', icon: 'data' },
    children: [
      {
        path: 'clientUser',
        component: () => import('@/views/clientUser/index.vue'),
        name: 'client_user',
        meta: { title: '客户端用户' }
      },
      {
        path: 'message',
        component: () => import('@/views/message/index.vue'),
        name: 'client_message',
        meta: { title: '询盘数据' }
      },
      {
        path: 'partner',
        component: () => import('@/views/partner/index.vue'),
        name: 'client_partner',
        meta: { title: '合作伙伴' }
      },
      {
        path: 'access',
        component: () => import('@/views/access/index.vue'),
        name: 'client_access',
        meta: { title: '访问统计' }
      }
    ]
  },
  {
    path: '/perm',
    component: shallowRef(Layout),
    name: 'perm',
    redirect: '/perm/users',
    meta: { title: '权限管理', icon: 'permission' },
    children: [
      {
        path: 'users',
        component: () => import('@/views/permission/users/index.vue'),
        name: 'perm_users',
        meta: { title: '系统用户' }
      },
      {
        path: 'depts',
        component: () => import('@/views/permission/depts/index.vue'),
        name: 'perm_depts',
        meta: { title: '部门管理' }
      },
      {
        path: 'posts',
        component: () => import('@/views/permission/posts/index.vue'),
        name: 'perm_posts',
        meta: { title: '岗位管理' }
      },
      {
        path: 'roles',
        component: () => import('@/views/permission/roles/index.vue'),
        name: 'perm_roles',
        meta: { title: '角色管理' }
      }
    ]
  },
  {
    path: '/system',
    component: shallowRef(Layout),
    meta: { title: '系统设置', icon: 'system' },
    name: 'system',
    redirect: '/system/menus',
    children: [
      {
        path: 'aiCode',
        component: () => import('@/views/aiCode/index.vue'),
        name: 'system_aiCode',
        meta: { title: 'AI生成代码' }
      },
      {
        path: 'setting',
        component: () => import('@/views/setting/index.vue'),
        name: 'system_setting',
        meta: { title: '系统设置' }
      },
      {
        path: 'webInfo',
        component: () => import('@/views/info/index.vue'),
        name: 'system_info',
        meta: { title: '网站信息' }
      },
      {
        path: 'menus',
        component: () => import('@/views/system/menus/index.vue'),
        name: 'system_menus',
        meta: { title: '菜单权限' }
      },
      {
        path: 'dict',
        component: () => import('@/views/system/dict/index.vue'),
        name: 'system_dict',
        meta: { title: '字典管理' }
      },
      {
        path: 'dict/data/:dictType',
        component: () => import('@/views/system/dict/data.vue'),
        name: 'system_dict_data',
        meta: { title: '字典数据管理', hidden: true }
      },
      {
        path: 'oss',
        component: () => import('@/views/system/oss/index.vue'),
        name: 'system_oss',
        meta: { title: '文件列表' }
      },
      {
        path: 'operalog',
        component: () => import('@/views/system/operalog/index.vue'),
        name: 'system_operalog',
        meta: { title: '操作日志' }
      },
      {
        path: 'loginlog',
        component: () => import('@/views/system/loginlog/index.vue'),
        name: 'system_loginlog',
        meta: { title: '登录日志' }
      },
    ]
  }
]
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: constantRoutes
})

export default router
