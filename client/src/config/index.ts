const env = import.meta.env

const config = {
  api: {
    baseUrl: `${env.VITE_APP_BASE_API_URL}`,
    websocketUrl: `${env.VITE_APP_WEBSOCKET_API_URL}`,
    staticUrl: `${env.VITE_APP_STATIC_API_URL}`,
    tmplDownloadUrl: `${env.VITE_APP_DOWNLOAD_URL}`
  },
  request: {
    timeout: `${env.VITE_APP_API_REQUEST_TIMEOUT}`
  }
}

export default config
