import { createApp } from 'vue'

// load
import { loadSvg } from './icons'
import { loadDirectives } from './directive'

import App from './App.vue'
import store from './store'
import router from './router'

import 'normalize.css/normalize.css'
import './styles/index.scss'

import './perm'

// 自己封装的一些组件
import KUI from './plugins/k-ui'

// highlight 的样式，依赖包，组件
import 'highlight.js/styles/atom-one-dark.css'
import 'highlight.js/lib/common'
import hljsVuePlugin from '@highlightjs/vue-plugin'

const app = createApp(App)
app.use(hljsVuePlugin)
/** 加载全局 SVG */
loadSvg(app)
/** 挂载自定义指令 */
loadDirectives(app)

// 封装的一些组件，扩展 element-plus 等
app.use(KUI)

app.use(store).use(router).mount('#app')
