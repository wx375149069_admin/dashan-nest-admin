import RSS from "rss"

export default defineEventHandler(async (event) => {
    try {
        const response = await fetch('banner/list/web')
        if (!response.ok) {
            throw new Error(response?.status?.toString())
        }
        const result = await response.json()
        const feed = new RSS({
            title: 'RYSEDE',
            site_url: 'https://site.com',
            feed_url: 'https://site.com/rss',
			image_url: 'https://site.com/favicon.ico',
			description: 'site website',
			copyright: 'Copyright © 2023 website',
			webMaster: 'website',
			managingEditor: 'website',
			language: 'zh-cn'
        });
        for (const post of result.data.list) {
            feed.item({
                title: post.title,
                url: `https://site.com/content/${post.id}`,
                description: post.intro,
                date: post.createTime,
                categories: [post.categoryName],
				author: 'site'
            })
        }
        const feedString = feed.xml({ indent: true })
        event.node.res.setHeader('content-type', 'text/xml')
        event.node.res.end(feedString)
    } catch (e) {
        return e
    }
});