// https://nuxt.com/docs/api/configuration/nuxt-config
import prismjs from 'vite-plugin-prismjs'
import Components from 'unplugin-vue-components/vite';
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers';
import { loadEnv } from 'vite'

export default defineNuxtConfig({
	runtimeConfig: {
		public: {
			apiBase: ''
		}
	},
    app: {
        head: {
            htmlAttrs: {
                lang: 'zh-CN'
            },
            script: [
                { src: '/scripts/vendor/modernizr-3.5.0.min.js', tagPosition: 'bodyClose' },
                { src: '/scripts/vendor/jquery-3.3.1.min.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/jquery.meanmenu.min.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/jquery.scrollUp.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/jquery.fancybox.min.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/jquery.countdown.min.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/jquery.nice-select.min.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/jquery-ui.min.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/owl.carousel.min.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/slick.min.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/popper.min.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/bootstrap.min.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/plugins.js', tagPosition: 'bodyClose' },
				{ src: '/scripts/main.js', tagPosition: 'bodyClose' }
            ],
            meta: [
                { charset: 'utf-8' },
                { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
                { name: 'apple-mobile-web-app-capable', content: 'yes' }
            ]
        }
    },
    css: [
        '@/assets/styles/font-awesome.min.css',
        '@/assets/styles/animate.css',
        '@/assets/styles/themify-icons.css',
		'@/assets/styles/stroke-gap.css',
		'@/assets/styles/material-design-iconic-font.min.css',
		'@/assets/styles/nice-select.css',
		'@/assets/styles/jquery.fancybox.css',
		'@/assets/styles/jquery-ui.min.css',
		'@/assets/styles/meanmenu.min.css',
		'@/assets/styles/owl.carousel.min.css',
		'@/assets/styles/slick.css',
		'@/assets/styles/bootstrap.min.css',
		'@/assets/styles/default.css',
		'@/assets/styles/style.css',
		'@/assets/styles/responsive.css',
    ],
	components: true,
	devtools: { enabled: true },
	devServer: {
		port: 5000
	},
	nitro: {
		routeRules: {
			'/api/**': {
				proxy: 'http://localhost:3000/**'
			}
		}
	},
	vite: {
		plugins: [
			prismjs({
				languages: [
					'css',
					'sass',
					'scss',
					'javascript',
					'html',
					'swift',
					'xml',
					'typescript',
					'dart',
					'csharp',
					'json',
					'sql',
					'bash',
					'md',
					'nginx',
					'cpp',
					'java'
				],
				plugins: ['toolbar', 'show-language', 'copy-to-clipboard'],
				theme: 'tomorrow',
				css: true
			}),
			// 'naive-ui'
			// Components({
            //     resolvers: [NaiveUiResolver()], // Automatically register all components in the `components` directory
            // }),
		],
		ssr: {
            noExternal: ['moment', 'naive-ui', '@juggle/resize-observer', '@css-render/vue3-ssr'],
        },
		optimizeDeps: {
			include:
			process.env.NODE_ENV === 'development'
			? ['naive-ui', 'vueuc', 'date-fns-tz/formatInTimeZone']
			: []
		}
	},
    experimental: {
        externalVue: false,
        inlineSSRStyles: false
    },
	build: {
		transpile:
		  process.env.NODE_ENV === 'production'
			? [
				'naive-ui',
				'vueuc',
				'@css-render/vue3-ssr',
				'@juggle/resize-observer'
			  ]
			: ['@juggle/resize-observer']
	}
})
