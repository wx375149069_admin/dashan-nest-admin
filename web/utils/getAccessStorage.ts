/**
 * 获取存储
 * @param {*} params  name、url
*/
export default (name: string, url: string) => {
	let data = localStorage.getItem(name)
	if (!data) {
		console.error(`输入的'${name}'存储名称错误或者存储的名称不存在`)
		return false
	}
	let obj = JSON.parse(data)
	if (new Date().getTime() >= obj.time && obj.data.url === url) {
		// 过期且访问url相同，则返回false，可重新存储
		return false
	} else if (new Date().getTime() <= obj.time && obj.data.url !== url) {
		// 不过期且访问url不相同，则返回false，可重新存储
		return false
	} else if (new Date().getTime() >= obj.time && obj.data.url !== url) {
		// 过期且访问url不相同，则返回false，可重新存储
		return false
	} else {
		return true
	}
}
