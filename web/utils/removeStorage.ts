/**
 * 删除存储
 * @param {*} params  name：无则清空全部
*/

export default (name: string) => {
	if (name) {
		localStorage.removeItem(name)
	} else {
		localStorage.clear()
	}
}
