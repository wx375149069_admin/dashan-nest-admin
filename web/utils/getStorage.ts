/**
 * 获取存储
 * @param {*} params  name
*/
export default (name: string) => {
	let data = localStorage.getItem(name)
	if (!data) {
		console.error(`输入的'${name}'存储名称错误或者存储的名称不存在`)
		return false
	}
	let obj = JSON.parse(data)
	if (new Date().getTime() > obj.time) { // 过期
		localStorage.removeItem(name)
		return false
	} else {
		return obj.data
	}
}
