/**
 * 赋予存储
 * @param {*} params  name、 value、 mins
*/

export default (name: string, value: object, mins?: number) => {
	let d = new Date()
	let time = 0
	mins = (typeof(mins) === 'undefined' || !mins) ? 10 : mins // 时间, 默认存储10分钟
	time = d.setMinutes(d.getMinutes() + mins) // 毫秒
	const setValue = {
		data: value,
		time: time
	}
	localStorage.setItem(name, JSON.stringify(setValue));
}
