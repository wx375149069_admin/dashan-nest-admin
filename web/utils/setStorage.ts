/**
 * 赋予存储
 * @param {*} params  name、 value、 day
*/

export default (name: string, value: object, day?: number) => {
	let d = new Date()
	let time = 0
	day = (typeof(day) === 'undefined' || !day) ? 1 : day // 时间, 默认存储1天
	time = d.setHours(d.getHours() + (24 * day)) // 毫秒
	const setValue = {
		data: value,
		time: time
	}
	localStorage.setItem(name, JSON.stringify(setValue));
}
