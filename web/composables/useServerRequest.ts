import { useFetch, UseFetchOptions, useRuntimeConfig } from '#app'
import IResponse from 'models/IResponse'

export const useServerRequest = async <T>(url: string, options?: UseFetchOptions<IResponse<T>>): Promise<T> => {
    // 环境api
    const appConfig = useRuntimeConfig()
    const baseURL = 'http://localhost:8081/api/'
    
    const opts: UseFetchOptions<IResponse<T>> = { baseURL: `${baseURL}`, ...options }
    const { data, error } = await useFetch<IResponse<T>>(url, opts)
    if (error.value) return Promise.reject(error.value.message)
    return data.value?.code === 200 ? Promise.resolve(data.value?.data) : Promise.reject(data.value?.msg)
}