interface IColumnThree {
    id: number
    title: string
    subhead: string
    type: string
    img: string
    keyword: string
    description: string
    content: string
    parentId: string,
    orderNum: number,
    createDate: string,
    updateDate: string,
    isDelete: number,
    children: Array<Object>
}

interface IColumnDetail {
    id: number
    title: string
    subhead: string
    type: string
    img: string
    keyword: string
    description: string
    content: string
    parentId: string,
    orderNum: number,
    createDate: string,
    updateDate: string,
    isDelete: number,
}

export type { IColumnThree, IColumnDetail }