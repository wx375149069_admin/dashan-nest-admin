export default interface IResponse<T> {
    code: number
    data: T
    msg: string
}