import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'

import { ProjectService } from './project.service'
import { ProjectController } from './project.controller'
import { ProjectEntity } from './entities/project.entity'
import { MonitoringLogEntity } from './entities/monitoring-log.entity'

@Module({
  imports: [TypeOrmModule.forFeature([ProjectEntity, MonitoringLogEntity])],
  controllers: [ProjectController],
  providers: [ProjectService],
})
export class ProjectModule {}
