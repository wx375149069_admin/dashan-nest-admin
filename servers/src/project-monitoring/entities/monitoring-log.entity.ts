import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'
import { ApiProperty } from '@nestjs/swagger'

@Entity('monitoring_log')
export class MonitoringLogEntity {
    @ApiProperty({ type: String, description: 'id' })
    @PrimaryGeneratedColumn({ type: 'bigint' })
    public id: string

    @ApiProperty({ type: String, description: '项目Id' })
    @Column({ type: 'varchar', name:'project_id', length: 255, comment: '项目Id' })
    public projectId: string

    @ApiProperty({ type: String, description: '项目名称' })
    @Column({ type: 'varchar', name:'project_name', length: 255, comment: '项目名称' })
    public projectName: string

    @ApiProperty({ type: String, description: '页面url' })
    @Column({ type: 'varchar', name:'page_url', length: 500, comment: '页面url' })
    public pageUrl: string

    @ApiProperty({ type: String, description: '接口地址' })
    @Column({ type: 'varchar', name:'api_url', length: 500, comment: '接口地址' })
    public apiUrl: string

    @ApiProperty({ type: String, description: '接口请求方式body/get' })
    @Column({ type: 'varchar', name:'api_method', length: 255, comment: '接口请求方式body/get' })
    public apiMethod: string

    @ApiProperty({ type: String, description: '接口请求参数' })
    @Column({ type: 'text', default: null, name:'api_params', comment: '接口请求参数' })
    public apiParams: string

    @ApiProperty({ type: String, description: '接口header参数' })
    @Column({ type: 'text', default: null, name:'api_header_params', comment: '接口header参数' })
    public apiHeaderParams: string

    @ApiProperty({ type: String, description: '后端返回的信息' })
    @Column({ type: 'text', default: null, name:'api_info', comment: '后端返回的信息' })
    public apiInfo: string

    @ApiProperty({ type: String, description: '用户id' })
    @Column({ type: 'varchar', name:'user_id', length: 255, comment: '客户端用户id' })
    public userId: string

    @ApiProperty({ type: String, description: '用户姓名/昵称' })
    @Column({ type: 'varchar', name:'user_name', length: 255, comment: '用户姓名/昵称' })
    public userName: string

    @ApiProperty({ type: Date, description: '创建时间' })
    @CreateDateColumn({ type: 'timestamp', name:'create_date', comment: '创建时间' })
    createDate: Date
}
