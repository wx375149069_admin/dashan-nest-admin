import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'
import { ApiProperty } from '@nestjs/swagger'
import { $enum } from 'ts-enum-util'
import { Exclude } from 'class-transformer'

import { StatusValue } from '../../common/enums/common.enum'

@Entity('project')
export class ProjectEntity {
    @ApiProperty({ type: String, description: 'id' })
    @PrimaryGeneratedColumn({ type: 'bigint' })
    public id: string

    @ApiProperty({ type: String, description: '项目名称' })
    @Column({ type: 'varchar', name:'project_name', length: 255, comment: '项目名称' })
    public projectName: string

    @ApiProperty({ type: String, description: '项目负责人' })
    @Column({ type: 'varchar', name:'user_name', length: 255, comment: '项目负责人' })
    public userName: string

    @ApiProperty({ type: String, description: '项目密钥' })
    @Column({ type: 'varchar', name:'project_key', length: 255, comment: '项目密钥' })
    public projectKey: string

    @ApiProperty({ type: String, description: '所属状态: 1-启动，0-禁用', enum: $enum(StatusValue).getValues() })
    @Column({ type: 'tinyint', name: 'status', default: 1, comment: '所属状态 1-启动，0-禁用' })
    public status: StatusValue

    @ApiProperty({ type: String, description: '所属状态: 1-不删除，0-删除', enum: $enum(StatusValue).getValues() })
    @Exclude({ toPlainOnly: true })
    @Column({ type: 'tinyint', name:'is_delete', default: StatusValue.NORMAL, comment: '所属删除逻辑: 1-不删除，0-删除' })
    public isDelete: StatusValue

    @ApiProperty({ type: Date, description: '创建时间' })
    @CreateDateColumn({ type: 'timestamp', name:'create_date', comment: '创建时间' })
    createDate: Date

    @ApiProperty({ type: Date, description: '更新时间' })
    @Exclude({ toPlainOnly: true })
    @UpdateDateColumn({ type: 'timestamp', name:'update_date', comment: '更新时间' })
    updateDate: Date
}
