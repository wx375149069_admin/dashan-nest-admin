import { ApiProperty } from '@nestjs/swagger'
import { ReqListQuery } from '../../common/utils/req-list-query'

import { $enum } from 'ts-enum-util'
import { StatusValue } from '../../common/enums/common.enum'

export class FindProjectListDto extends ReqListQuery {
  @ApiProperty({ description: '项目模糊搜索', required: false })
  projectName?: string

  @ApiProperty({ description: '按启动禁用状态查询', enum: $enum(StatusValue).getValues(), required: false })
  status?: StatusValue
}
