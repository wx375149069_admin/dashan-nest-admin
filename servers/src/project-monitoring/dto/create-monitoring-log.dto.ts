import { ApiProperty } from '@nestjs/swagger'
import { IsMobilePhone, IsNotEmpty, IsString } from 'class-validator'

export class CreateMonitoringLogDto {
    @ApiProperty({ description: '项目id' })
    projectId: string

    @ApiProperty({ description: '项目名称' })
    projectName: string

    @ApiProperty({ description: '页面url' })
    @IsNotEmpty({ message: '页面url不能为空' })
    readonly pageUrl: string

    @ApiProperty({ description: '接口地址' })
    @IsNotEmpty({ message: '接口地址不能为空' })
    readonly apiUrl: string

    @ApiProperty({ description: '接口请求参数' })
    @IsNotEmpty({ message: '接口请求参数不能为空' })
    readonly apiParams: string

    @ApiProperty({ description: '接口header参数' })
    @IsNotEmpty({ message: '接口header参数不能为空' })
    readonly apiHeaderParams: string

    @ApiProperty({ description: '后端返回的信息' })
    @IsNotEmpty({ message: '后端返回的信息不能为空' })
    readonly apiInfo: string

    @ApiProperty({ description: '用户id' })
    readonly userId: string

    @ApiProperty({ description: '用户姓名/昵称' })
    readonly userName: string
}
