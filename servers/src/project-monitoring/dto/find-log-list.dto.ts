import { ApiProperty } from '@nestjs/swagger'
import { ReqListQuery } from '../../common/utils/req-list-query'

import { $enum } from 'ts-enum-util'
import { StatusValue } from '../../common/enums/common.enum'

export class FindLogListDto extends ReqListQuery {
  @ApiProperty({ description: '项目名称模糊搜索', required: false })
  projectName?: string

  @ApiProperty({ description: '创建日期', required: false })
  createDate?: string
}
