import { ApiProperty } from '@nestjs/swagger'
import { IsMobilePhone, IsNotEmpty, IsString } from 'class-validator'

export class CreateProjectDto {
    @ApiProperty({ description: '项目名称' })
    @IsNotEmpty({ message: '项目名称不能为空' })
    readonly projectName: string

    @ApiProperty({ description: '项目密钥key' })
    projectKey: string
}
