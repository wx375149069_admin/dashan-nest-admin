import { ApiProperty } from '@nestjs/swagger'
import { IsMobilePhone, IsNotEmpty, IsOptional, IsString, IsNumberString } from 'class-validator'

export class UpdateProjectDto {
    @ApiProperty({ description: '项目编码' })
    @IsNumberString({}, { message: 'id 类型错误，正确类型 string' })
    @IsNotEmpty({ message: 'id 不能为空' })
    readonly id: string

    @ApiProperty({ description: '项目名称' })
    @IsNotEmpty({ message: '项目名称不能为空' })
    readonly projectName: string

    @ApiProperty({ description: '项目密钥key' })
    @IsNotEmpty({ message: '项目密钥key不能为空' })
    readonly projectKey: string
}
