import { Controller, Query, Get, Post, Body, Patch, Param, Delete, Req, Put, Ip } from '@nestjs/common';
import { ProjectService } from './project.service';
import { ApiTags, ApiOperation, ApiQuery, ApiBearerAuth } from '@nestjs/swagger'

import { ProjectEntity } from './entities/project.entity'
import { MonitoringLogEntity } from './entities/monitoring-log.entity'

import { CreateProjectDto } from './dto/create-project.dto'
import { UpdateProjectDto } from './dto/update-project.dto'
import { UpdateStatusDto } from './dto/update-status.dto'
import { UpdateDeleteDto } from './dto/update-delete.dto'
import { FindProjectListDto } from './dto/find-project-list.dto'
import { FindLogListDto } from './dto/find-log-list.dto'

import { ResultData } from '../common/utils/result'
import { ApiResult } from '../common/decorators/api-result.decorator'

@ApiTags('项目监控模块')
@Controller('projectMonitoring')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Get('list')
  @ApiOperation({ summary: '查询项目列表' })
  @ApiResult(ProjectEntity, true, true)
  async findList(@Query() dto: FindProjectListDto): Promise<ResultData> {
    return await this.projectService.findList(dto)
  }

  @Post()
  @ApiOperation({ summary: '创建项目' })
  @ApiResult()
  async create(@Body() createProjectDto: CreateProjectDto, @Req() req, @Ip() ip) {
    return await this.projectService.create(createProjectDto, req.user.account, ip);
  }

  @Put()
  @ApiOperation({ summary: '更新项目' })
  @ApiResult()
  async update(@Body() updateProjectDto: UpdateProjectDto, @Req() req, @Ip() ip) {
    return await this.projectService.create(updateProjectDto, req.user.account, ip);
  }

  @Get('info')
  @ApiOperation({ summary: '根据id查询项目信息' })
  @ApiQuery({ name: 'id' })
  @ApiResult(ProjectEntity)
  async findOne(@Query('id') id: string): Promise<ResultData> {
    return this.projectService.findOne(id);
  }

  @Put('/status/change')
  @ApiOperation({ summary: '更改项目状态' })
  @ApiResult()
  async updateStatus(@Body() dto: UpdateStatusDto, @Req() req, @Ip() ip): Promise<ResultData> {
    return await this.projectService.updateStatus(dto.id, dto.status, req.user.account, ip)
  }

  @Delete('/delete')
  @ApiOperation({ summary: '逻辑删除项目' })
  @ApiResult()
  async delete(@Body() dto: UpdateDeleteDto, @Req() req, @Ip() ip): Promise<ResultData> {
    return await this.projectService.updateDelete(dto.id, dto.isDelete, req.user.account, ip)
  }

  @Get('log/list')
  @ApiOperation({ summary: '查询监控日志列表' })
  @ApiResult(ProjectEntity, true, true)
  async findLogList(@Query() dto: FindLogListDto): Promise<ResultData> {
    return await this.projectService.findLogList(dto)
  }

  @Get('logOne')
  @ApiOperation({ summary: '查询单条监控日志' })
  @ApiQuery({ name: 'id' })
  @ApiResult(MonitoringLogEntity)
  async findLogOne(@Query() id: { id: string }): Promise<ResultData> {
    return this.projectService.findLogOne(id.id)
  }
}
