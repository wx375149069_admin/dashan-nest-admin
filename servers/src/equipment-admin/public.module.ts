import { Module, forwardRef } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'

// 客户
import { CustomerService } from './customer.service'
import { CustomerController } from './customer.controller'
import { CustomerEntity } from './entities/customer.entity'

// 设备
import { EquipmentEntity } from './entities/equipment.entity'
import { EquipmentController } from './equipment.controller'
import { EquipmentService } from './equipment.service'

// 供应商
import { SupplierService } from './supplier.service'
import { SupplierEntity } from './entities/supplier.entity'
import { SupplierController } from './supplier.controller'

@Module({
  imports: [TypeOrmModule.forFeature([CustomerEntity, EquipmentEntity, SupplierEntity])],
  controllers: [CustomerController, SupplierController, EquipmentController],
  providers: [CustomerService, SupplierService, EquipmentService],
})
export class EquipmentAdminModule {}
