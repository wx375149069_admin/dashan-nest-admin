import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'
import { ApiProperty } from '@nestjs/swagger'
import { $enum } from 'ts-enum-util'
import { Exclude } from 'class-transformer'
import { StatusValue } from '../../common/enums/common.enum'

@Entity('supplier')
export class SupplierEntity {
    @ApiProperty({ type: String, description: 'id' })
    @PrimaryGeneratedColumn({ type: 'bigint' })
    public id: string

    @ApiProperty({ type: String, description: '供应商名称' })
    @Column({ type: 'varchar', name: 'name', length: 500, comment: '供应商名称' })
    public name: string

    @ApiProperty({ type: String, description: '供应商联系人' })
    @Column({ type: 'varchar', name: 'user_name', length: 255, comment: '供应商联系人' })
    public userName: string

    @ApiProperty({ type: String, description: '供应商手机号' })
    @Column({ type: 'varchar', name: 'phone', length: 255, comment: '供应商手机号' })
    public phone: string

    @ApiProperty({ type: String, description: '供应商邮箱' })
    @Column({ type: 'varchar', name: 'email', length: 255, comment: '供应商邮箱' })
    public email: string

    @ApiProperty({ type: String, description: '供应商地址' })
    @Column({ type: 'varchar', name: 'address', length: 800, comment: '供应商地址' })
    public address: string

    @ApiProperty({ type: String, description: '备注' })
    @Column({ type: 'varchar', name: 'remark', length: 1000, comment: '备注' })
    public remark: string

    @ApiProperty({ type: Date, description: '创建时间' })
    @CreateDateColumn({ type: 'timestamp', name:'create_date', comment: '创建时间' })
    createDate: Date

    @ApiProperty({ type: Date, description: '更新时间' })
    @Exclude({ toPlainOnly: true })
    @UpdateDateColumn({ type: 'timestamp', name:'update_date', comment: '更新时间' })
    updateDate: Date

    @ApiProperty({ type: String, description: '所属状态: 1-不删除，0-删除', enum: $enum(StatusValue).getValues() })
    @Exclude({ toPlainOnly: true })
    @Column({ type: 'tinyint', name:'is_delete', default: StatusValue.NORMAL, comment: '所属删除逻辑: 1-不删除，0-删除' })
    public isDelete: StatusValue
}
