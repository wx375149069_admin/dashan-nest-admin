import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'
import { ApiProperty } from '@nestjs/swagger'
import { $enum } from 'ts-enum-util'
import { Exclude } from 'class-transformer'
import { StatusValue, AuditValue } from '../../common/enums/common.enum'

@Entity('equipment')
export class EquipmentEntity {
    @ApiProperty({ type: String, description: 'id' })
    @PrimaryGeneratedColumn({ type: 'bigint' })
    public id: string

    @ApiProperty({ type: String, description: '硬件名称' })
    @Column({ type: 'varchar', name: 'name', length: 600, comment: '硬件名称' })
    public name: string

    @ApiProperty({ type: String, description: '数量' })
    @Column({ type: 'varchar', name: 'number', length: 255, comment: '数量' })
    public number: string

    @ApiProperty({ type: String, description: '客户id' })
    @Column({ type: 'varchar', name: 'customer_id', length: 255, comment: '客户地址' })
    public customerId: string

    @ApiProperty({ type: String, description: '客户姓名' })
    @Column({ type: 'varchar', name: 'customer_name', length: 255, comment: '客户姓名' })
    public customerName: string

    @ApiProperty({ type: String, description: '供应商id' })
    @Column({ type: 'varchar', name: 'supplier_id', length: 500, comment: '供应商id' })
    public supplierId: string

    @ApiProperty({ type: String, description: '供应商名称' })
    @Column({ type: 'varchar', name: 'supplier_name', length: 255, comment: '供应商名称' })
    public supplierName: string

    @ApiProperty({ type: String, description: '客户手机号' })
    @Column({ type: 'varchar', name: 'phone', length: 255, comment: '客户手机号' })
    public phone: string

    @ApiProperty({ type: String, description: '收货地址' })
    @Column({ type: 'varchar', name: 'address', length: 800, comment: '收货地址' })
    public address: string

    @ApiProperty({ type: Date, description: '下单时间' })
    @UpdateDateColumn({ type: 'timestamp', name:'order_date', comment: '下单时间' })
    orderDate: Date

    @ApiProperty({ type: String, description: '技术负责人' })
    @Column({ type: 'varchar', name: 'technology_charge_person', length: 255, comment: '技术负责人' })
    public technologyChargePerson: string

    @ApiProperty({ type: String, description: '采购负责人' })
    @Column({ type: 'varchar', name: 'purchase_charge_person', length: 255, comment: '采购负责人' })
    public purchaseChargePerson: string

    @ApiProperty({ type: String, description: '审核人' })
    @Column({ type: 'varchar', name: 'examine_person', length: 255, comment: '审核人' })
    public examinePerson: string

    @ApiProperty({ type: String, description: '订单类型' })
    @Column({ type: 'varchar', name: 'type', length: 255, comment: '订单类型' })
    public type: string

    @ApiProperty({ type: String, description: '快递公司编码' })
    @Column({ type: 'varchar', name: 'courier_company', length: 255, comment: '快递公司编码' })
    public courierCompany: string

    @ApiProperty({ type: String, description: '快递公司名称' })
    @Column({ type: 'varchar', name: 'courier_company_name', length: 255, comment: '快递公司名称' })
    public courierCompanyName: string

    @ApiProperty({ type: String, description: '快递单号' })
    @Column({ type: 'varchar', name: 'courier_number', length: 400, comment: '快递单号' })
    public courierNumber: string

    @ApiProperty({ type: String, description: '备注' })
    @Column({ type: 'varchar', name: 'remark', length: 1000, comment: '备注' })
    public remark: string

    @ApiProperty({ type: String, description: '审核备注' })
    @Column({ type: 'varchar', name: 'auditRemark', length: 1000, comment: '审核备注' })
    public auditRemark: String

    @ApiProperty({ type: Date, description: '创建时间' })
    @CreateDateColumn({ type: 'timestamp', name:'create_date', comment: '创建时间' })
    createDate: Date

    @ApiProperty({ type: Date, description: '更新时间' })
    @Exclude({ toPlainOnly: true })
    @UpdateDateColumn({ type: 'timestamp', name:'update_date', comment: '更新时间' })
    updateDate: Date

    @ApiProperty({ type: String, description: '所属状态: 1-审核通过，0-审核不通过，2审核中', enum: $enum(AuditValue).getValues() })
    @Column({ type: 'tinyint', name:'status', default: AuditValue.NORMAL, comment: '所属状态: 1-审核通过，0-审核不通过，2审核中' })
    public status: AuditValue

    @ApiProperty({ type: String, description: '所属状态: 1-不删除，0-删除', enum: $enum(StatusValue).getValues() })
    @Exclude({ toPlainOnly: true })
    @Column({ type: 'tinyint', name:'is_delete', default: StatusValue.NORMAL, comment: '所属删除逻辑: 1-不删除，0-删除' })
    public isDelete: StatusValue
}
