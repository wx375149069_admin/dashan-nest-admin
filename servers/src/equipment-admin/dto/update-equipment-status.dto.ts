import { ApiProperty } from '@nestjs/swagger'
import { IsNumber, IsNotEmpty, IsIn, IsString } from 'class-validator'
import { $enum } from 'ts-enum-util'

import { AuditValue } from '../../common/enums/common.enum'

export class UpdateEquipmentStatusDto {
  @ApiProperty({ description: '编码' })
  @IsString({ message: 'id 类型错误，正确类型 string' })
  @IsNotEmpty({ message: 'id 不能为空' })
  readonly id: string

  @ApiProperty({ description: '所属状态:  1-审核通过，0-审核不通过，2审核中', enum: $enum(AuditValue).getValues() })
  @IsNumber({}, { message: 'status 类型错误，正确类型 number' })
  @IsNotEmpty({ message: 'status 不能为空' })
  @IsIn([0, 1, 2], { message: 'status 可选值0/1/2，分别表示审核不通过、审核通过、审核中' })
  readonly status: AuditValue

  @ApiProperty({ description: '审核备注' })
  @IsNotEmpty({ message: '审核备注不能为空' })
  readonly auditRemark: String
}
