import { ApiProperty } from '@nestjs/swagger'
import { IsMobilePhone, IsNotEmpty, IsOptional, IsString, IsNumberString } from 'class-validator'

export class UpdateCustomerDto {
    @ApiProperty({ description: '编码' })
    @IsNumberString({}, { message: 'id 类型错误，正确类型 string' })
    @IsNotEmpty({ message: 'id 不能为空' })
    readonly id: string

    @ApiProperty({ description: '客户姓名' })
    @IsNotEmpty({ message: '客户姓名不能为空' })
    readonly name: string

    @ApiProperty({ description: '客户手机号', required: false })
    @IsString({ message: '手机号类型错误，正确类型-字符串' })
    @IsMobilePhone('zh-CN', { strictMode: false }, { message: '请输入正确的手机号' })
    @IsOptional()
    readonly phone?: string

    @ApiProperty({ description: '客户地址', required: false })
    @IsString({ message: '客户地址类型错误，正确类型-字符串' })
    @IsOptional()
    readonly address?: string
}
