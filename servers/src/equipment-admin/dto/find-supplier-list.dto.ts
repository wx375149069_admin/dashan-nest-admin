import { ApiProperty } from '@nestjs/swagger'
import { ReqListQuery } from '../../common/utils/req-list-query'
import { StatusValue } from '../../common/enums/common.enum'

export class FindSupplierListDto extends ReqListQuery {
  @ApiProperty({ description: '供应商名称模糊搜索', required: false })
  name?: string

  @ApiProperty({ description: '供应商联系人模糊搜索', required: false })
  userName?: string

  @ApiProperty({ description: '供应商手机号模糊搜索', required: false })
  phone?: StatusValue
}
