import { ApiProperty } from '@nestjs/swagger'
import { ReqListQuery } from '../../common/utils/req-list-query'
import { StatusValue } from '../../common/enums/common.enum'

export class FindCustomerListDto extends ReqListQuery {
  @ApiProperty({ description: '客户姓名模糊搜索', required: false })
  name?: string

  @ApiProperty({ description: '客户手机号模糊搜索', required: false })
  phone?: StatusValue
}
