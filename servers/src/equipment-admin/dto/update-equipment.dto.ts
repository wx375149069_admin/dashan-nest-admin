import { ApiProperty } from '@nestjs/swagger'
import { IsMobilePhone, IsNotEmpty, IsOptional, IsString, IsNumberString } from 'class-validator'

export class UpdateEquipmentDto {
    @ApiProperty({ description: '编码' })
    @IsNumberString({}, { message: 'id 类型错误，正确类型 string' })
    @IsNotEmpty({ message: 'id 不能为空' })
    readonly id: string

    @ApiProperty({ description: '硬件名称' })
    @IsNotEmpty({ message: '硬件名称不能为空' })
    readonly name: string

    @ApiProperty({ description: '客户姓名' })
    @IsNotEmpty({ message: '客户姓名不能为空' })
    readonly customerName: string

    @ApiProperty({ description: '供应商名称' })
    @IsNotEmpty({ message: '供应商名称不能为空' })
    readonly supplierName: string

    @ApiProperty({ description: '客户手机号', required: false })
    @IsString({ message: '手机号类型错误，正确类型-字符串' })
    @IsMobilePhone('zh-CN', { strictMode: false }, { message: '请输入正确的手机号' })
    @IsOptional()
    readonly phone?: string

    @ApiProperty({ description: '收货地址', required: false })
    @IsString({ message: '收货地址类型错误，正确类型-字符串' })
    @IsOptional()
    readonly address?: string

    @ApiProperty({ description: '下单时间' })
    @IsNotEmpty({ message: '下单时间不能为空' })
    readonly orderDate: string
    
    @ApiProperty({ description: '技术负责人' })
    @IsNotEmpty({ message: '技术负责人不能为空' })
    readonly technologyChargePerson: string

    @ApiProperty({ description: '采购负责人' })
    @IsNotEmpty({ message: '采购负责人不能为空' })
    readonly purchaseChargePerson: string

    @ApiProperty({ description: '审核负责人' })
    @IsNotEmpty({ message: '审核负责人不能为空' })
    readonly examinePerson: string

    @ApiProperty({ description: '订单类型' })
    @IsNotEmpty({ message: '订单类型不能为空' })
    readonly type: string
}
