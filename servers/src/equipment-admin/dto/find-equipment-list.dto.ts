import { ApiProperty } from '@nestjs/swagger'
import { ReqListQuery } from '../../common/utils/req-list-query'
import { StatusValue } from '../../common/enums/common.enum'

export class FindEquipmentListDto extends ReqListQuery {
  @ApiProperty({ description: '硬件名称模糊搜索', required: false })
  name?: string

  @ApiProperty({ description: '客户id精确搜索', required: false })
  customerId?: string

  @ApiProperty({ description: '客户名称模糊搜索', required: false })
  customerName?: string

  @ApiProperty({ description: '供应商模糊搜索', required: false })
  supplierName?: string

  @ApiProperty({ description: '技术负责人模糊搜索', required: false })
  technologyChargePerson?: string

  @ApiProperty({ description: '采购负责人模糊搜索', required: false })
  purchaseChargePerson?: string

  @ApiProperty({ description: '审核负责人模糊搜索', required: false })
  examinePerson?: string

  @ApiProperty({ description: '客户手机号模糊搜索', required: false })
  phone?: string

  @ApiProperty({ description: '订单类型搜索', required: false })
  type?: string

  @ApiProperty({ description: '下单日期', required: false })
  orderDate?: string
}
