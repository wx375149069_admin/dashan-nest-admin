import { ApiProperty } from '@nestjs/swagger'
import { IsEmail, IsMobilePhone, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator'

export class CreateSupplierDto {
    @ApiProperty({ description: '供应商名称' })
    @IsNotEmpty({ message: '供应商名称不能为空' })
    readonly name: string

    @ApiProperty({ description: '供应商联系人' })
    @IsNotEmpty({ message: '供应商联系人不能为空' })
    readonly userName: string

    @ApiProperty({ description: '供应商手机号', required: false })
    @IsString({ message: '手机号类型错误，正确类型-字符串' })
    @IsMobilePhone('zh-CN', { strictMode: false }, { message: '请输入正确的手机号' })
    @IsOptional()
    readonly phone?: string

    @ApiProperty({ description: '供应商地址', required: false })
    @IsString({ message: '供应商地址类型错误，正确类型-字符串' })
    @IsOptional()
    readonly address?: string
}
