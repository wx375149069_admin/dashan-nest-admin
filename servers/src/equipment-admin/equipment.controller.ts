import { Controller, Query, Get, Post, Body, Patch, Param, Delete, Req, Put, Ip } from '@nestjs/common'
import { EquipmentService } from './equipment.service'
import { ApiTags, ApiOperation, ApiBody, ApiConsumes, ApiQuery, ApiBearerAuth } from '@nestjs/swagger'
import { EquipmentEntity } from './entities/equipment.entity'

import { CreateEquipmentDto } from './dto/create-equipment.dto'
import { UpdateEquipmentDto } from './dto/update-equipment.dto'
import { UpdateDeleteDto } from './dto/update-delete.dto'
import { UpdateEquipmentStatusDto } from './dto/update-equipment-status.dto'
import { FindEquipmentListDto } from './dto/find-equipment-list.dto'

import { ResultData } from '../common/utils/result'
import { ApiResult } from '../common/decorators/api-result.decorator'

@ApiTags('硬件录入模块')
@ApiBearerAuth()
@Controller('equipment')
export class EquipmentController {
  constructor(private readonly equipmentService: EquipmentService) {}

  @Get('list')
  @ApiOperation({ summary: '查询硬件录入列表' })
  @ApiResult(EquipmentEntity, true, true)
  async findList(@Query() dto: FindEquipmentListDto): Promise<ResultData> {
    return await this.equipmentService.findList(dto)
  }

  @Post()
  @ApiOperation({ summary: '创建硬件录入信息' })
  @ApiResult()
  async create(@Body() createEquipmentDto: CreateEquipmentDto, @Req() req, @Ip() ip) {
    return await this.equipmentService.create(createEquipmentDto, req.user.account, ip);
  }

  @Get('info')
  @ApiOperation({ summary: '根据id查询硬件录入信息' })
  @ApiQuery({ name: 'id' })
  @ApiResult(EquipmentEntity)
  async findOne(@Query('id') id: string): Promise<ResultData> {
    return this.equipmentService.findOne(id);
  }

  @Put()
  @ApiOperation({ summary: '更新硬件录入信息' })
  @ApiResult()
  async update(@Body() updateEquipmentDto: UpdateEquipmentDto, @Req() req, @Ip() ip) {
    return await this.equipmentService.update(updateEquipmentDto, req.user.account, ip);
  }

  @Put('/status/change')
  @ApiOperation({ summary: '更改硬件录入状态' })
  @ApiResult()
  async updateStatus(@Body() dto: UpdateEquipmentStatusDto, @Req() req, @Ip() ip): Promise<ResultData> {
    return await this.equipmentService.updateStatus(dto.id, dto.status, dto.auditRemark, req.user.account, ip)
  }

  @Delete('/delete')
  @ApiOperation({ summary: '逻辑删除硬件录入' })
  @ApiResult()
  async delete(@Body() dto: UpdateDeleteDto, @Req() req, @Ip() ip): Promise<ResultData> {
    return await this.equipmentService.updateDelete(dto.id, dto.isDelete, req.user.account, ip)
  }

  @Get('express')
  @ApiOperation({ summary: '根据id查询硬件快递信息' })
  @ApiQuery({ name: 'id' })
  @ApiResult(EquipmentEntity)
  async findExpress(@Query('id') id: string): Promise<ResultData> {
    return this.equipmentService.findExpress(id);
  }
}
