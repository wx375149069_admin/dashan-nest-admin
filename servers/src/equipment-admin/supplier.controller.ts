import { Controller, Query, Get, Post, Body, Patch, Param, Delete, Req, Put, Ip } from '@nestjs/common'
import { SupplierService } from './supplier.service'
import { ApiTags, ApiOperation, ApiBody, ApiConsumes, ApiQuery, ApiBearerAuth } from '@nestjs/swagger'
import { SupplierEntity } from './entities/supplier.entity'

import { CreateSupplierDto } from './dto/create-supplier.dto'
import { UpdateSupplierDto } from './dto/update-supplier.dto'
import { UpdateDeleteDto } from './dto/update-delete.dto'
import { FindSupplierListDto } from './dto/find-supplier-list.dto'

import { ResultData } from '../common/utils/result'
import { ApiResult } from '../common/decorators/api-result.decorator'

@ApiTags('供应商模块')
@ApiBearerAuth()
@Controller('supplier')
export class SupplierController {
  constructor(private readonly supplierService: SupplierService) {}

  @Get('list')
  @ApiOperation({ summary: '查询供应商列表' })
  @ApiResult(SupplierEntity, true, true)
  async findList(@Query() dto: FindSupplierListDto): Promise<ResultData> {
    return await this.supplierService.findList(dto)
  }

  @Post()
  @ApiOperation({ summary: '创建供应商信息' })
  @ApiResult()
  async create(@Body() createSupplierDto: CreateSupplierDto, @Req() req, @Ip() ip) {
    return await this.supplierService.create(createSupplierDto, req.user.account, ip);
  }

  @Get('info')
  @ApiOperation({ summary: '根据id查询供应商信息' })
  @ApiQuery({ name: 'id' })
  @ApiResult(SupplierEntity)
  async findOne(@Query('id') id: string): Promise<ResultData> {
    return this.supplierService.findOne(id);
  }

  @Put()
  @ApiOperation({ summary: '更新供应商信息' })
  @ApiResult()
  async update(@Body() updateSupplierDto: UpdateSupplierDto, @Req() req, @Ip() ip) {
    return await this.supplierService.update(updateSupplierDto, req.user.account, ip);
  }

  @Delete('/delete')
  @ApiOperation({ summary: '逻辑删除供应商' })
  @ApiResult()
  async delete(@Body() dto: UpdateDeleteDto, @Req() req, @Ip() ip): Promise<ResultData> {
    return await this.supplierService.updateDelete(dto.id, dto.isDelete, req.user.account, ip)
  }

}
