import { Controller, Query, Get, Post, Body, Patch, Param, Delete, Req, Put, Ip } from '@nestjs/common';
import { CustomerService } from './customer.service';
import { ApiTags, ApiOperation, ApiBody, ApiConsumes, ApiQuery, ApiBearerAuth } from '@nestjs/swagger'
import { CustomerEntity } from './entities/customer.entity';

import { CreateCustomerDto } from './dto/create-customer.dto'
import { UpdateCustomerDto } from './dto/update-customer.dto'
import { UpdateDeleteDto } from './dto/update-delete.dto'
import { FindCustomerListDto } from './dto/find-customer-list.dto'

import { ResultData } from '../common/utils/result'
import { ApiResult } from '../common/decorators/api-result.decorator'

@ApiTags('客户硬件模块')
@ApiBearerAuth()
@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Get('list')
  @ApiOperation({ summary: '查询客户列表' })
  @ApiResult(CustomerEntity, true, true)
  async findList(@Query() dto: FindCustomerListDto): Promise<ResultData> {
    return await this.customerService.findList(dto)
  }

  @Post()
  @ApiOperation({ summary: '创建客户信息' })
  @ApiResult()
  async create(@Body() createCustomerDto: CreateCustomerDto, @Req() req, @Ip() ip) {
    return await this.customerService.create(createCustomerDto, req.user.account, ip);
  }

  @Get('info')
  @ApiOperation({ summary: '根据id查询客户信息' })
  @ApiQuery({ name: 'id' })
  @ApiResult(CustomerEntity)
  async findOne(@Query('id') id: string): Promise<ResultData> {
    return this.customerService.findOne(id);
  }

  @Put()
  @ApiOperation({ summary: '更新客户信息' })
  @ApiResult()
  async update(@Body() updateCustomerDto: UpdateCustomerDto, @Req() req, @Ip() ip) {
    return await this.customerService.update(updateCustomerDto, req.user.account, ip);
  }

  @Delete('/delete')
  @ApiOperation({ summary: '逻辑删除客户' })
  @ApiResult()
  async delete(@Body() dto: UpdateDeleteDto, @Req() req, @Ip() ip): Promise<ResultData> {
    return await this.customerService.updateDelete(dto.id, dto.isDelete, req.user.account, ip)
  }

}
