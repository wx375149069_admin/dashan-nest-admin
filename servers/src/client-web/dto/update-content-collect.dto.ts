import { ApiProperty } from '@nestjs/swagger'
import { IsMobilePhone, IsNotEmpty, IsOptional, IsString, } from 'class-validator'

export class UpdateContentCollectDto {
    @ApiProperty({ description: '内容id 集合' })
    @IsNotEmpty({ each: true, message: '内容id集合中存在为空' })
    readonly contentId: string

    @ApiProperty({ description: '客户端用户id 集合' })
    @IsNotEmpty({ each: true, message: '客户端用户id集合中存在为空' })
    readonly userId: string

    @ApiProperty({ description: '客户端用户姓名 集合' })
    @IsNotEmpty({ each: true, message: '客户端用户姓名集合中存在为空' })
    readonly userName: string

    @ApiProperty({ description: '内容收藏量' })
    readonly collect: number
}
