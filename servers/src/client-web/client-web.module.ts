import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'

import { InfoEntity } from '../system/info/entities/info.entity'
import { BannerEntity } from '../home/entities/banner.entity'
import { HomeContentEntity } from '../home/entities/home-content.entity';
import { HomeContentListEntity } from '../home/entities/home-content-list.entity'
import { UserEntity } from '../client/clientuser/entities/clientuser.entity'
import { ColumnEntity } from '../content/entities/column.entity'
import { ContentEntity } from '../content/entities/content.entity'
import { TagsEntity } from '../content/entities/tags.entity'
import { ContentTagsEntity } from '../content/entities/content-tags.entity'
import { ContentCollectEntity } from '../content/entities/content-collect.entity'
import { PartnerEntity } from '../partner/entities/partner.entity'
import { MessageEntity } from '../message/entities/message.entity'
import { AccessEntity } from '../client/access/entities/access.entity'
import { MonitoringLogEntity } from '../project-monitoring/entities/monitoring-log.entity'
import { ProjectEntity } from '../project-monitoring/entities/project.entity'

import { HomeService } from './home.service';
import { ContentTagsService } from '../content/content-tags.service'
import { ClientWebController } from './client-web.controller';

@Module({
  imports: [TypeOrmModule.forFeature([
    InfoEntity,
    BannerEntity,
    HomeContentEntity,
    HomeContentListEntity,
    UserEntity,
    ColumnEntity,
    ContentEntity,
    TagsEntity,
    ContentTagsEntity,
    PartnerEntity,
    MessageEntity,
    ContentCollectEntity,
    AccessEntity,
    ProjectEntity,
    MonitoringLogEntity])],
  controllers: [ClientWebController],
  providers: [HomeService, ContentTagsService ],
})
export class ClientWebModule {}
