import { Controller, Get, Query, Body, Post, Req, Param, Put, Ip } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiQuery } from '@nestjs/swagger'

import { ResultData } from '../common/utils/result'
import { ApiResult } from '../common/decorators/api-result.decorator'

import { HomeService } from './home.service';

import { CreateClientUserDto } from '../client/clientuser/dto/create-clientuser.dto'
import { LoginUser } from '../client/clientuser/dto/login-clientuser.dto'
import { CreateMessageDto } from '../message/dto/create-message.dto'
import { UpdateContentCollectDto } from './dto/update-content-collect.dto'
import { UpdateContentConsultDto } from './dto/update-content-consult.dto'
import { CreateAccessDto } from '../client/access/dto/create-access.dto'
import { CreateMonitoringLogDto } from '../project-monitoring/dto/create-monitoring-log.dto'

import { ColumnEntity } from '../content/entities/column.entity'
import { HomeContentEntity } from '../home/entities/home-content.entity'
import { PartnerEntity } from '../partner/entities/partner.entity'


@ApiTags('客户端网站')
@Controller('clientWeb')
export class ClientWebController {
  constructor(private readonly homeService: HomeService) {}

  /** 查询网站信息 */
  @Get('webInfo')
  @ApiOperation({ summary: '查询网站信息' })
  @ApiResult()
  async findWebInfo() {
    return this.homeService.findWebInfo();
  }

  /** 查询栏目树形列表 */
  @Get('columnAllList')
  @ApiOperation({ summary: '查询栏目树形列表' })
  @ApiResult(ColumnEntity, true, true)
  async findColumnAllList(): Promise<ResultData> {
    return await this.homeService.findColumnAllList()
  }

  /** 根据模板查找栏目 */
  @Get('column/tempType/:template')
  @ApiOperation({ summary: '根据模板查找栏目' })
  @ApiResult(ColumnEntity)
  async findColumnSiblings(@Param('template') template: string): Promise<ResultData> {
    return this.homeService.findColumnSiblings(template);
  }

  /** 根据模板查找栏目内容并以map对象返回 */
  @Get('column/tempType/content/:template')
  @ApiOperation({ summary: '根据模板查找栏目' })
  @ApiResult(ColumnEntity)
  async findColumnSiblingsContnet(@Param('template') template: string): Promise<ResultData> {
    return this.homeService.findColumnSiblingsContnet(template);
  }

  /** 根据id查询栏目信息 */
  @Get('column/:id')
  @ApiOperation({ summary: '根据id查询栏目信息' })
  @ApiResult(ColumnEntity)
  async findColumnOne(@Param('id') id: string): Promise<ResultData> {
    return this.homeService.findColumnOne(id);
  }

  /** 查询首页banner所有 */
  @Get('homeBanner')
  @ApiOperation({ summary: '查询首页banner' })
  @ApiResult()
  async findHomeBanner() {
    return this.homeService.findHomeBannerList();
  }

  /** 查询首页内容列表信息
   * code: 首页内容标识
   */
  @Get('homeContent/:code')
  @ApiOperation({ summary: '查询首页内容（带列表）' })
  @ApiResult(HomeContentEntity)
  async findHomeContent(@Param('code') code: string): Promise<ResultData> {
    return this.homeService.findHomeContent(code);
  }

  /** 客户端用户注册*/
  @Post('register')
  @ApiOperation({ summary: '客户端用户注册' })
  @ApiResult()
  async findClientUserRegister(@Body() dto: CreateClientUserDto): Promise<ResultData> {
    return this.homeService.createClientUser(dto);
  }

  /** 客户端用户登录*/
  @Post('login')
  @ApiOperation({ summary: '客户端用户登录' })
  @ApiResult()
  async findClientUserLogin(@Body() dto: LoginUser): Promise<ResultData> {
    return this.homeService.clientUserLogin(dto.account, dto.password);
  }

  /** 根据栏目id查询最近3篇内容 */
  @Get('recentList/:id')
  @ApiOperation({ summary: '根据栏目id查询最近3篇内容' })
  @ApiResult()
  async findContentRecent(@Param('id') id: string): Promise<ResultData> {
    return this.homeService.findContentRecent(id);
  }

  /** 根据栏目id分页查询内容信息 */
  @Get('column/content/:id/:page/:size')
  @ApiOperation({ summary: '根据栏目id分页查询内容信息' })
  @ApiResult()
  async findColumnContent(@Param('id') id: string, @Param('page') page: string, @Param('size') size: string): Promise<ResultData> {
    return this.homeService.findContentList(id, parseInt(page), parseInt(size));
  }

  /** 根据标题模糊分页查询内容信息 */
  @Get('column/contentSearch/:page/:size')
  @ApiOperation({ summary: '根据栏目id分页查询内容信息' })
  @ApiQuery({ name: 'search' })
  @ApiResult()
  async findColumnContentSearch(@Query('search') search: string, @Param('page') page: string, @Param('size') size: string): Promise<ResultData> {
    return this.homeService.findContentSearchList(search, parseInt(page), parseInt(size));
  }

  /** 根据最新分页查询内容信息 */
  @Get('contentNew/:size')
  @ApiOperation({ summary: '根据最新分页查询内容信息' })
  @ApiQuery({ name: 'search' })
  @ApiResult()
  async findColumnContentNew(@Param('size') size: string): Promise<ResultData> {
    return this.homeService.findColumnContentNew(parseInt(size));
  }

  /** 根据推荐分页查询内容信息 */
  @Get('contentRecomm/:code')
  @ApiOperation({ summary: '根据栏目id分页查询内容信息' })
  @ApiQuery({ name: 'search' })
  @ApiResult()
  async findColumnContentRecomm(@Param('code') code: string): Promise<ResultData> {
    return this.homeService.findContentRecommList(code);
  }

  /** 根据内容id内容信息 */
  @Get('content/:id')
  @ApiOperation({ summary: '根据内容id内容信息' })
  @ApiResult()
  async findContentOne(@Param('id') id: string): Promise<ResultData> {
    return this.homeService.findContentOne(id);
  }

  /** 获取推荐产品 */
  @Get('recomProduct')
  @ApiOperation({ summary: '获取推荐产品' })
  @ApiResult()
  async findRecomProduct(): Promise<ResultData> {
    return this.homeService.findRecomProduct();
  }

  /** 查询合作伙伴列表 */
  @Get('partnerlist')
  @ApiOperation({ summary: '查询合作伙伴列表' })
  @ApiResult(PartnerEntity, true, true)
  async findPartnerList(): Promise<ResultData> {
    return await this.homeService.findPartnerList()
  }

  /** 提交留言*/
  @Post('submitMessage')
  @ApiOperation({ summary: '客户端提交留言' })
  @ApiResult()
  async submitMessage(@Body() dto: CreateMessageDto, @Ip() ip): Promise<ResultData> {
    return this.homeService.submitMessage(dto, ip);
  }

  /** 根据用户Id和内容是否收藏 */
  @Get('contentIsCollect/:contentId/:userId')
  @ApiOperation({ summary: '根据用户Id和内容是否收藏' })
  @ApiResult()
  async getContentIsCollect(@Param('contentId') contentId: string, @Param('userId') userId: string): Promise<ResultData> {
    return await this.homeService.getContentIsCollect(contentId, userId)
  }

  /** 更新内容收藏量 */
  @Put('content/collect')
  @ApiOperation({ summary: '更新内容收藏量' })
  @ApiResult()
  async updateContentCollect(@Body() dto: UpdateContentCollectDto): Promise<ResultData> {
    return await this.homeService.updateContentCollect(dto)
  }

  /** 更新内容咨询量 */
  @Post('content/common')
  @ApiOperation({ summary: '更新内容咨询量' })
  @ApiResult()
  async updateContentConsult(@Body() dto: UpdateContentConsultDto, @Ip() ip): Promise<ResultData> {
    return await this.homeService.updateContentConsult(dto, ip)
  }

  /** 更新页面访问量 */
  @Post('access')
  @ApiOperation({ summary: '更新页面访问量' })
  @ApiResult()
  async createAccess(@Body() createAccessDto: CreateAccessDto, @Ip() ip): Promise<ResultData> {
    return await this.homeService.createAccess(createAccessDto, ip);
  }

  /** 新增项目监控 */
  @Post('monitoring/:key')
  @ApiOperation({ summary: '项目监控' })
  @ApiResult()
  async createMonitoring(@Body() createMonitoringLogDto: CreateMonitoringLogDto, @Param('key') key: string): Promise<ResultData> {
    return await this.homeService.createMonitoringLog(createMonitoringLogDto, key);
  }
}
