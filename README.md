# NestJs大山后台开发管理系统

#### 介绍
NestJs大山后台开发管理系统，是为了前端人员方便快速开发后台管理系统而研发并开源，采用NestJs为后端开发框架，Vue3+Ts+Vite为前端框架，并遵循MIT协议，里面包含以下功能：

1、QQ邮箱发送；

2、阿里云sms短信；

3、阿里云oss文件上传；

4、每日定时备份数据库；

5、星火AI人工智能大模型；

6、websocket；

7、操作/登录日志；

8、RBAC角色权限；

9、Sse（Server-Send Events）单向消息提醒和发送；

10、快递鸟物流信息对接；

11、AI生成前后端代码；

12、一些简单业务流；(包括AI内容回填业务数据)


#### 后台演示

地址：http://43.136.28.22:5173/

演示账号：demoUser

演示密码：123456

演示管理员账号：admin

演示管理员密码：dashan375!@


#### 前后端框架和环境系统版本

 **前端：** 

1、Vue3;（后台前端框架）

2、Typescript;

3、Element-plus（后台UI框架）;

4、Nuxt3（前台框架）；


 **后端：** 

1、node >= 16.18.0版本；

2、npm 8.0版本；

3、NestJs 10.0.3版本；

4、Redis 5.0版本 / 7.0版本；

5、MySql  8.0版本；





#### 使用说明

 **一：后台前端client文件夹：** 

```
 安装依赖：npm install

 启动项目：npm run dev

 项目打包：npm run build
```


 **二：项目后端servers文件夹：** 

```
安装依赖：pnpm install

启动项目：npm run start:dev

项目打包：npm run build

打包后启动：npm run start:prod
```

 **三：客户端官网web文件夹：** 


```
安装依赖：pnpm install

启动项目：npm run dev

项目打包：npm run build

打包后启动：node .output/server/index.mjs  （这个建议使用PM2启动）
```


 **温馨提示：** 后端服务servers和客户端官网前端web，尽量用pnpm命令安装依赖。


注意：此系统可以二次开发及商用，但禁止二次开发色情、暴力、赌博及任何违法国家法律的软件，一旦发现，本人将有权提起法律诉讼！

#### 版本说明

1、1.0.0 系统发布

2、1.1.0 基本版本bug修复

3、2.0.0 添加AI基础业务流、其它bug修复

4、2.1.0 优化页面及权限

备注：由于是个人开发开源，所以更新会比较慢，如果有其它特殊功能或者有兴趣一起开发，可以加QQ或者微信联系我，我给你添加开发权限


#### 参与贡献

作者：大山 QQ：375149069

是基于nest-admin二次开发开源的，[点击我跳转nest-admin](https://gitee.com/wenqiyun/nest-admin)

 **加入QQ群** 

![输入图片说明](%E5%A4%A7%E5%B1%B1AI%E8%90%A5%E9%94%80%E5%BC%80%E6%BA%90%E7%B3%BB%E7%BB%9F%E7%BE%A4%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

#### 后台部分截图 

备注：代码上的业务功能比截图的多，想预览更多，请下载/拉取代码自行查看

![输入图片说明](11.jpg)

![输入图片说明](22.jpg)

![输入图片说明](33.jpg)

![输入图片说明](44.jpg)

麻烦大家点击个Start！感谢！

