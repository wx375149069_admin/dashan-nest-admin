/*
 Navicat Premium Data Transfer

 Source Server         : dashan
 Source Server Type    : MySQL
 Source Server Version : 80024
 Source Host           : 43.136.28.22:3306
 Source Schema         : dashan

 Target Server Type    : MySQL
 Target Server Version : 80024
 File Encoding         : 65001

 Date: 29/03/2024 09:19:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for client_access
-- ----------------------------
DROP TABLE IF EXISTS `client_access`;
CREATE TABLE `client_access`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户端用户id',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户端用户姓名，无则是游客',
  `user_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户端用户邮箱',
  `access_page` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访问页面',
  `source_site` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '来源网站',
  `district` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '地区',
  `access_equipment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访问设备',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `content_id` bigint NOT NULL COMMENT '内容id',
  `is_content` tinyint NOT NULL DEFAULT 0 COMMENT '所属状态: 1-内容页，0-不是内容页',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for client_user
-- ----------------------------
DROP TABLE IF EXISTS `client_user`;
CREATE TABLE `client_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户登录密码',
  `salt` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '盐',
  `account` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户登录账号',
  `sex` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `phone_num` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户手机号码',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '地址',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-有效，0-禁用',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for column
-- ----------------------------
DROP TABLE IF EXISTS `column`;
CREATE TABLE `column`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '栏目标题',
  `subhead` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '栏目副标题',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'page' COMMENT '栏目类型',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '栏目banner',
  `keyword` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '栏目seo关键词',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '栏目seo描述',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '栏目内容',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  `order_num` int NOT NULL COMMENT '栏目排序',
  `parent_id` bigint NOT NULL COMMENT '栏目父级id',
  `template` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '栏目模板',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for content
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `recom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容推荐/头条',
  `publish_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容发布时间',
  `img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容封面图片组',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容详情',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-展示，0-隐藏',
  `column_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容栏目',
  `order_num` int NOT NULL COMMENT '内容排序',
  `price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容价格',
  `original_price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容原价',
  `read_num` int NOT NULL DEFAULT 0 COMMENT '访问量',
  `title` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容标题',
  `keyword` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容seo关键词',
  `description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容seo描述',
  `consult_num` int NOT NULL DEFAULT 0 COMMENT '咨询量',
  `intro` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容简介',
  `collect_num` int NOT NULL DEFAULT 0 COMMENT '收藏量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for content_collect
-- ----------------------------
DROP TABLE IF EXISTS `content_collect`;
CREATE TABLE `content_collect`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `content_id` bigint NOT NULL COMMENT '内容id',
  `user_id` bigint NOT NULL COMMENT '客户端用户id',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户姓名',
  `collect` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-收藏，0-没收藏',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for content_tags
-- ----------------------------
DROP TABLE IF EXISTS `content_tags`;
CREATE TABLE `content_tags`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `content_id` bigint NOT NULL COMMENT '内容id',
  `tags_id` bigint NOT NULL COMMENT '标签id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户姓名',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户手机号',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  `address` varchar(800) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户地址',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户邮箱',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for equipment
-- ----------------------------
DROP TABLE IF EXISTS `equipment`;
CREATE TABLE `equipment`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '硬件名称',
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数量',
  `customer_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户地址',
  `customer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户姓名',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户手机号',
  `address` varchar(800) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '收货地址',
  `order_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '下单时间',
  `technology_charge_person` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '技术负责人',
  `purchase_charge_person` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '采购负责人',
  `examine_person` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '审核人',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '订单类型',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '备注',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-审核通过，0-审核不通过，2审核中',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  `supplier_id` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '供应商id',
  `supplier_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '供应商名称',
  `auditRemark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '审核备注',
  `courier_company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '快递公司编码',
  `courier_number` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '快递单号',
  `courier_company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '快递公司名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for home_banner
-- ----------------------------
DROP TABLE IF EXISTS `home_banner`;
CREATE TABLE `home_banner`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描述',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '跳转链接',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'banner图',
  `wap_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '移动端banner图',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-展示，0-隐藏',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for home_content
-- ----------------------------
DROP TABLE IF EXISTS `home_content`;
CREATE TABLE `home_content`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描述',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '跳转链接',
  `code` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容标识',
  `imgs` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图片组合',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-展示，0-隐藏',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for home_content_list
-- ----------------------------
DROP TABLE IF EXISTS `home_content_list`;
CREATE TABLE `home_content_list`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描述',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '跳转链接',
  `imgs` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图片组合',
  `content_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '首页内容Id',
  `content_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '首页内容标识',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-展示，0-隐藏',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '电话号码',
  `status` tinyint NOT NULL DEFAULT 0 COMMENT '阅读状态 1-已读，0-未读',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户姓名/游客',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱',
  `message_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '留言ip',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '留言地区',
  `content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '留言内容',
  `message_url` varchar(550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '留言来源',
  `content_name` varchar(550) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '留言内容名称/无则为空',
  `content_id` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '留言内容id/无则为空',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for monitoring_log
-- ----------------------------
DROP TABLE IF EXISTS `monitoring_log`;
CREATE TABLE `monitoring_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目Id',
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目名称',
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户端用户id',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户姓名/昵称',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `api_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '接口请求方式body/get',
  `api_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '接口请求参数',
  `page_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '页面url',
  `api_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '接口地址',
  `api_header_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '接口header参数',
  `api_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '后端返回的信息',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for partner
-- ----------------------------
DROP TABLE IF EXISTS `partner`;
CREATE TABLE `partner`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '合作伙伴logo',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-展示，0-隐藏',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `partner_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '合作伙伴名称',
  `partner_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '合作伙伴手机号',
  `partner_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '合作伙伴联系人',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目名称',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目负责人',
  `project_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目密钥',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态 1-启动，0-禁用',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '供应商名称',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '供应商手机号',
  `address` varchar(800) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '供应商地址',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '供应商联系人',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '供应商邮箱',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `parent_id` bigint NOT NULL COMMENT '父级部门 id',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '部门状态，1-有效，0-禁用',
  `order_num` int NOT NULL DEFAULT 0 COMMENT '排序',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '备注',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '部门名称',
  `leader` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '部门负责人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `dict_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
  `dict_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
  `description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典描述',
  `content` varchar(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典内容',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-有效，0-禁用',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `dict_label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `dict_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '键值',
  `description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描述',
  `content` varchar(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-有效，0-禁用',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  `dict_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典Id',
  `dict_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_loginlog
-- ----------------------------
DROP TABLE IF EXISTS `sys_loginlog`;
CREATE TABLE `sys_loginlog`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名称',
  `login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录IP',
  `login_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录地点',
  `login_browser` varchar(510) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录浏览器',
  `login_system` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录系统',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-成功，0-失败',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '菜单名称',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '菜单/按钮唯一标识，由前端路由name,用于控制菜单按钮显隐',
  `type` int NOT NULL COMMENT '菜单类型， 1-菜单/目录 2-tabs 3-按钮',
  `order_num` int NOT NULL DEFAULT 0 COMMENT '排序',
  `parent_id` bigint NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 119 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '首页', 'dashboard', 1, 99, 0);
INSERT INTO `sys_menu` VALUES (2, '权限管理', 'perm', 1, 1, 0);
INSERT INTO `sys_menu` VALUES (3, '用户管理', 'perm_users', 1, 0, 2);
INSERT INTO `sys_menu` VALUES (4, '角色管理', 'perm_roles', 1, 0, 2);
INSERT INTO `sys_menu` VALUES (5, '系统设置', 'system', 1, 0, 0);
INSERT INTO `sys_menu` VALUES (6, '菜单权限', 'system_menus', 1, 5, 5);
INSERT INTO `sys_menu` VALUES (7, '文件列表', 'system_oss', 1, 4, 5);
INSERT INTO `sys_menu` VALUES (8, '编辑', 'perm_users:edit', 3, 0, 3);
INSERT INTO `sys_menu` VALUES (9, '启用/禁用', 'perm_users:updateStatus', 3, 0, 3);
INSERT INTO `sys_menu` VALUES (10, '重置密码', 'perm_users:resetPw', 3, 0, 3);
INSERT INTO `sys_menu` VALUES (11, '批量导入', 'perm_users:createMultUser', 3, 0, 3);
INSERT INTO `sys_menu` VALUES (12, '新增', 'perm_roles:create', 3, 0, 4);
INSERT INTO `sys_menu` VALUES (13, '编辑', 'perm_roles:edit', 3, 0, 4);
INSERT INTO `sys_menu` VALUES (14, '删除', 'perm_roles:del', 3, 0, 4);
INSERT INTO `sys_menu` VALUES (15, '关联用户/解除关联', 'perm_roles:bind', 3, 0, 4);
INSERT INTO `sys_menu` VALUES (16, '添加', 'system_menus:create', 3, 0, 6);
INSERT INTO `sys_menu` VALUES (17, '编辑', 'system_menus:edit', 3, 0, 6);
INSERT INTO `sys_menu` VALUES (18, '删除', 'system_menus:del', 3, 0, 6);
INSERT INTO `sys_menu` VALUES (25, '部门管理', 'perm_depts', 1, 0, 2);
INSERT INTO `sys_menu` VALUES (26, '岗位管理', 'perm_posts', 1, 0, 2);
INSERT INTO `sys_menu` VALUES (27, '新增', 'perm_posts:create', 3, 0, 26);
INSERT INTO `sys_menu` VALUES (28, '编辑', 'perm_posts:edit', 3, 0, 26);
INSERT INTO `sys_menu` VALUES (29, '删除', 'perm_posts:del', 3, 0, 26);
INSERT INTO `sys_menu` VALUES (30, '删除', 'perm_depts:del', 3, 0, 25);
INSERT INTO `sys_menu` VALUES (31, '编辑', 'perm_depts:edit', 3, 0, 25);
INSERT INTO `sys_menu` VALUES (32, '新增', 'perm_depts:create', 3, 0, 25);
INSERT INTO `sys_menu` VALUES (33, '添加', 'perm_users:add', 3, 0, 3);
INSERT INTO `sys_menu` VALUES (34, '首页AI管理', 'home', 1, 5, 0);
INSERT INTO `sys_menu` VALUES (35, '合作伙伴', 'client_partner', 1, 1, 83);
INSERT INTO `sys_menu` VALUES (36, '添加', 'partner:add', 3, 0, 35);
INSERT INTO `sys_menu` VALUES (37, '编辑', 'partner:edit', 3, 0, 35);
INSERT INTO `sys_menu` VALUES (38, '更新状态', 'partner:updateStatus', 3, 0, 35);
INSERT INTO `sys_menu` VALUES (39, '删除', 'partner:delete', 3, 0, 35);
INSERT INTO `sys_menu` VALUES (40, '网站信息', 'system_info', 1, 6, 5);
INSERT INTO `sys_menu` VALUES (41, '更新', 'system:info:update', 3, 0, 40);
INSERT INTO `sys_menu` VALUES (42, '询盘数据', 'client_message', 1, 2, 83);
INSERT INTO `sys_menu` VALUES (43, '查看留言', 'message:see', 3, 0, 42);
INSERT INTO `sys_menu` VALUES (44, '删除留言', 'message:delete', 3, 0, 42);
INSERT INTO `sys_menu` VALUES (45, '操作日志', 'system_operalog', 1, 2, 5);
INSERT INTO `sys_menu` VALUES (46, '登录日志', 'system_loginlog', 1, 1, 5);
INSERT INTO `sys_menu` VALUES (47, '字典管理', 'system_dict', 1, 3, 5);
INSERT INTO `sys_menu` VALUES (48, '新增', 'system:dict:add', 3, 0, 47);
INSERT INTO `sys_menu` VALUES (49, '编辑', 'system:dict:edit', 3, 0, 47);
INSERT INTO `sys_menu` VALUES (50, '删除', 'system:dict:delete', 3, 0, 47);
INSERT INTO `sys_menu` VALUES (51, '状态更新', 'system:dict:updateStatus', 3, 0, 47);
INSERT INTO `sys_menu` VALUES (52, '字典数据禁用', 'system:dict:data:updateStatus', 3, 0, 47);
INSERT INTO `sys_menu` VALUES (53, '字典数据删除', 'system:dict:data:delete', 3, 0, 47);
INSERT INTO `sys_menu` VALUES (54, '字典数据编辑', 'system:dict:data:edit', 3, 0, 47);
INSERT INTO `sys_menu` VALUES (55, '字典数据新增', 'system:dict:data:add', 3, 0, 47);
INSERT INTO `sys_menu` VALUES (56, 'AI轮播图', 'home_banner', 1, 0, 34);
INSERT INTO `sys_menu` VALUES (57, '新增', 'home:banner:add', 3, 0, 56);
INSERT INTO `sys_menu` VALUES (58, '编辑', 'home:banner:edit', 3, 0, 56);
INSERT INTO `sys_menu` VALUES (59, '删除', 'home:banner:delete', 3, 0, 56);
INSERT INTO `sys_menu` VALUES (60, '更新状态', 'home:banner:updateStatus', 3, 0, 56);
INSERT INTO `sys_menu` VALUES (61, '自定义AI内容', 'home_content', 1, 0, 34);
INSERT INTO `sys_menu` VALUES (62, '新增', 'home:content:add', 3, 0, 61);
INSERT INTO `sys_menu` VALUES (63, '修改', 'home:content:edit', 3, 0, 61);
INSERT INTO `sys_menu` VALUES (64, '删除', 'home:content:delete', 3, 0, 61);
INSERT INTO `sys_menu` VALUES (65, '查看列表', 'home:content:list', 3, 0, 61);
INSERT INTO `sys_menu` VALUES (66, '更新状态', 'home:content:updateStatus', 3, 0, 61);
INSERT INTO `sys_menu` VALUES (67, '系统设置', 'system_setting', 1, 7, 5);
INSERT INTO `sys_menu` VALUES (68, '内容管理', 'content', 1, 4, 0);
INSERT INTO `sys_menu` VALUES (69, 'AI标签管理', 'content_tags', 1, 1, 68);
INSERT INTO `sys_menu` VALUES (70, '新增', 'tags:add', 3, 0, 69);
INSERT INTO `sys_menu` VALUES (71, '编辑', 'tags:edit', 3, 0, 69);
INSERT INTO `sys_menu` VALUES (72, '删除', 'tags:delete', 3, 0, 69);
INSERT INTO `sys_menu` VALUES (73, '更新状态', 'tags:updateStatus', 3, 0, 69);
INSERT INTO `sys_menu` VALUES (74, 'AI栏目管理', 'content_column', 1, 0, 68);
INSERT INTO `sys_menu` VALUES (75, '新增', 'column:add', 3, 0, 74);
INSERT INTO `sys_menu` VALUES (76, '编辑', 'column:edit', 3, 0, 74);
INSERT INTO `sys_menu` VALUES (77, '删除', 'column:delete', 3, 0, 74);
INSERT INTO `sys_menu` VALUES (78, 'AI内容管理', 'content_content', 1, 2, 68);
INSERT INTO `sys_menu` VALUES (79, '新增', 'content:add', 3, 0, 78);
INSERT INTO `sys_menu` VALUES (80, '编辑', 'content:edit', 3, 0, 78);
INSERT INTO `sys_menu` VALUES (81, '删除内容', 'content:delete', 3, 0, 78);
INSERT INTO `sys_menu` VALUES (82, '更新状态', 'content:updateStatus', 3, 0, 78);
INSERT INTO `sys_menu` VALUES (83, '客户端数据', 'client', 1, 3, 0);
INSERT INTO `sys_menu` VALUES (84, '客户端用户', 'client_user', 1, 3, 83);
INSERT INTO `sys_menu` VALUES (85, '查看', 'clientUser:see', 3, 0, 84);
INSERT INTO `sys_menu` VALUES (86, '更改状态', 'clientUser:updateStatus', 3, 0, 84);
INSERT INTO `sys_menu` VALUES (87, '访问统计', 'client_access', 1, 0, 83);
INSERT INTO `sys_menu` VALUES (89, '查看', 'access:see', 3, 0, 87);
INSERT INTO `sys_menu` VALUES (90, '营销数据', 'content:statistics', 3, 0, 78);
INSERT INTO `sys_menu` VALUES (91, '发送邮件', 'clientUser:sendMail', 3, 0, 84);
INSERT INTO `sys_menu` VALUES (92, 'AI生成', 'tag:ai', 3, 0, 69);
INSERT INTO `sys_menu` VALUES (93, '项目监控', 'projectMonitoring', 1, 6, 0);
INSERT INTO `sys_menu` VALUES (94, '项目管理', 'project_admin', 1, 1, 93);
INSERT INTO `sys_menu` VALUES (95, '新增', 'project:admin:add', 3, 0, 94);
INSERT INTO `sys_menu` VALUES (96, '修改', 'project:admin:edit', 3, 0, 94);
INSERT INTO `sys_menu` VALUES (97, '删除', 'project:admin:delete', 3, 0, 94);
INSERT INTO `sys_menu` VALUES (98, '更改状态', 'project:admin:updateStatus', 3, 0, 94);
INSERT INTO `sys_menu` VALUES (99, '监控日志', 'project_log', 1, 0, 93);
INSERT INTO `sys_menu` VALUES (100, 'AI生成代码', 'system_aiCode', 1, 8, 5);
INSERT INTO `sys_menu` VALUES (101, '客户硬件管理', 'equipmentAdmin', 1, 7, 0);
INSERT INTO `sys_menu` VALUES (102, '客户管理', 'equipment_customer', 1, 1, 101);
INSERT INTO `sys_menu` VALUES (103, '新增', 'equipment:customer:add', 3, 0, 102);
INSERT INTO `sys_menu` VALUES (104, '编辑', 'equipment:customer:edit', 3, 0, 102);
INSERT INTO `sys_menu` VALUES (105, '删除', 'equipment:customer:delete', 3, 0, 102);
INSERT INTO `sys_menu` VALUES (106, '供应商管理', 'equipment_supplier', 1, 0, 101);
INSERT INTO `sys_menu` VALUES (107, '新增', 'equipment:supplier:add', 3, 0, 106);
INSERT INTO `sys_menu` VALUES (108, '编辑', 'equipment:supplier:edit', 3, 0, 106);
INSERT INTO `sys_menu` VALUES (109, '删除', 'equipment:supplier:delete', 3, 0, 106);
INSERT INTO `sys_menu` VALUES (110, '硬件录入管理', 'equipment_index', 1, 0, 101);
INSERT INTO `sys_menu` VALUES (111, '新增', 'equipment:index:add', 3, 0, 110);
INSERT INTO `sys_menu` VALUES (112, '修改', 'equipment:index:edit', 3, 0, 110);
INSERT INTO `sys_menu` VALUES (113, '删除', 'equipment:index:delete', 3, 0, 110);
INSERT INTO `sys_menu` VALUES (114, '更改状态', 'equipment:index:updateStatus', 3, 0, 110);
INSERT INTO `sys_menu` VALUES (115, '总询盘数据', 'home:message', 3, 0, 1);
INSERT INTO `sys_menu` VALUES (116, '获取过去12个月的访问数据', 'home:month', 3, 0, 1);
INSERT INTO `sys_menu` VALUES (117, '内容统计', 'home:access', 3, 0, 1);
INSERT INTO `sys_menu` VALUES (118, '查询客户端用户列表所有', 'home:user', 3, 0, 1);

-- ----------------------------
-- Table structure for sys_menu_perm
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_perm`;
CREATE TABLE `sys_menu_perm`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `menu_id` bigint NOT NULL COMMENT '菜单id',
  `api_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '该菜单所能调用的 api 接口，必须是本应用的接口，否则设置了也不生效',
  `api_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '该菜单所能调用 api 接口的 method 方法',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 849 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu_perm
-- ----------------------------
INSERT INTO `sys_menu_perm` VALUES (10, 8, '/api/user/one/:id', 'GET');
INSERT INTO `sys_menu_perm` VALUES (11, 8, '/api/user', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (12, 9, '/api/user/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (13, 10, '/api/user/password/reset/:userId', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (15, 12, '/api/role', 'POST');
INSERT INTO `sys_menu_perm` VALUES (16, 13, '/api/role', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (17, 14, '/api/role/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (18, 15, '/api/user/role/update', 'POST');
INSERT INTO `sys_menu_perm` VALUES (20, 17, '/api/menu', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (21, 16, '/api/menu', 'POST');
INSERT INTO `sys_menu_perm` VALUES (22, 18, '/api/menu/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (24, 11, '/api/user/import', 'POST');
INSERT INTO `sys_menu_perm` VALUES (32, 29, '/api/post/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (36, 27, '/api/post', 'POST');
INSERT INTO `sys_menu_perm` VALUES (37, 28, '/api/post/:id', 'GET');
INSERT INTO `sys_menu_perm` VALUES (38, 28, '/api/post', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (39, 30, '/api/dept/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (40, 31, '/api/dept', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (41, 32, '/api/dept', 'POST');
INSERT INTO `sys_menu_perm` VALUES (88, 25, '/api/dept/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (89, 25, '/api/register', 'POST');
INSERT INTO `sys_menu_perm` VALUES (90, 25, '/api/login', 'POST');
INSERT INTO `sys_menu_perm` VALUES (91, 25, '/api/update/token', 'POST');
INSERT INTO `sys_menu_perm` VALUES (92, 25, '/api/user/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (93, 25, '/api/user/one/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (94, 25, '/api/user/:id/role', 'GET');
INSERT INTO `sys_menu_perm` VALUES (95, 25, '/api/user/role/update', 'POST');
INSERT INTO `sys_menu_perm` VALUES (96, 25, '/api/user', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (97, 25, '/api/user/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (98, 25, '/api/user/password/reset/:userId', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (99, 25, '/api/user/import', 'POST');
INSERT INTO `sys_menu_perm` VALUES (100, 25, '/api/perm/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (101, 25, '/api/perm/user', 'GET');
INSERT INTO `sys_menu_perm` VALUES (102, 25, '/api/perm/menu', 'GET');
INSERT INTO `sys_menu_perm` VALUES (103, 25, '/api/menu/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (104, 25, '/api/menu/one/:parentId/btns', 'GET');
INSERT INTO `sys_menu_perm` VALUES (105, 25, '/api/menu/one/:id/menu-perm', 'GET');
INSERT INTO `sys_menu_perm` VALUES (106, 25, '/api/menu', 'POST');
INSERT INTO `sys_menu_perm` VALUES (107, 25, '/api/menu', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (108, 25, '/api/menu/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (109, 25, '/api/role/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (110, 25, '/api/role/one/:id/perms', 'GET');
INSERT INTO `sys_menu_perm` VALUES (111, 25, '/api/role', 'POST');
INSERT INTO `sys_menu_perm` VALUES (112, 25, '/api/role', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (113, 25, '/api/role/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (114, 25, '/api/dept', 'POST');
INSERT INTO `sys_menu_perm` VALUES (115, 25, '/api/dept', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (116, 25, '/api/dept/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (117, 25, '/api/post', 'POST');
INSERT INTO `sys_menu_perm` VALUES (118, 25, '/api/post', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (119, 25, '/api/post/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (120, 25, '/api/post/:id', 'GET');
INSERT INTO `sys_menu_perm` VALUES (121, 25, '/api/post/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (122, 25, '/api/oss/upload', 'POST');
INSERT INTO `sys_menu_perm` VALUES (123, 25, '/api/oss/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (124, 26, '/api/post/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (125, 26, '/api/post', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (126, 26, '/api/post', 'POST');
INSERT INTO `sys_menu_perm` VALUES (127, 26, '/api/dept/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (128, 26, '/api/post/:id', 'GET');
INSERT INTO `sys_menu_perm` VALUES (129, 26, '/api/post/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (130, 26, '/api/oss/upload', 'POST');
INSERT INTO `sys_menu_perm` VALUES (131, 26, '/api/oss/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (132, 26, '/api/register', 'POST');
INSERT INTO `sys_menu_perm` VALUES (133, 26, '/api/login', 'POST');
INSERT INTO `sys_menu_perm` VALUES (134, 26, '/api/update/token', 'POST');
INSERT INTO `sys_menu_perm` VALUES (135, 26, '/api/user/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (136, 26, '/api/user/one/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (137, 26, '/api/user/:id/role', 'GET');
INSERT INTO `sys_menu_perm` VALUES (138, 26, '/api/user/role/update', 'POST');
INSERT INTO `sys_menu_perm` VALUES (139, 26, '/api/user', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (140, 26, '/api/user/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (141, 26, '/api/user/password/reset/:userId', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (142, 26, '/api/user/import', 'POST');
INSERT INTO `sys_menu_perm` VALUES (143, 26, '/api/perm/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (144, 26, '/api/perm/user', 'GET');
INSERT INTO `sys_menu_perm` VALUES (145, 26, '/api/perm/menu', 'GET');
INSERT INTO `sys_menu_perm` VALUES (146, 26, '/api/menu/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (147, 26, '/api/menu/one/:parentId/btns', 'GET');
INSERT INTO `sys_menu_perm` VALUES (148, 26, '/api/menu/one/:id/menu-perm', 'GET');
INSERT INTO `sys_menu_perm` VALUES (149, 26, '/api/menu', 'POST');
INSERT INTO `sys_menu_perm` VALUES (150, 26, '/api/menu', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (151, 26, '/api/menu/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (152, 26, '/api/role/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (153, 26, '/api/role/one/:id/perms', 'GET');
INSERT INTO `sys_menu_perm` VALUES (154, 26, '/api/role', 'POST');
INSERT INTO `sys_menu_perm` VALUES (155, 26, '/api/role', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (156, 26, '/api/role/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (157, 26, '/api/dept', 'POST');
INSERT INTO `sys_menu_perm` VALUES (158, 26, '/api/dept', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (159, 26, '/api/dept/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (232, 4, '/api/role/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (233, 4, '/api/role/one/:id/perms', 'GET');
INSERT INTO `sys_menu_perm` VALUES (234, 4, '/api/user/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (235, 4, '/api/role', 'POST');
INSERT INTO `sys_menu_perm` VALUES (236, 4, '/api/role', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (237, 4, '/api/role/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (238, 4, '/api/dept', 'POST');
INSERT INTO `sys_menu_perm` VALUES (239, 4, '/api/dept', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (240, 4, '/api/dept/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (241, 4, '/api/dept/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (242, 4, '/api/post', 'POST');
INSERT INTO `sys_menu_perm` VALUES (243, 4, '/api/post', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (244, 4, '/api/post/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (245, 4, '/api/post/:id', 'GET');
INSERT INTO `sys_menu_perm` VALUES (246, 4, '/api/post/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (247, 4, '/api/oss/upload', 'POST');
INSERT INTO `sys_menu_perm` VALUES (248, 4, '/api/oss/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (249, 4, '/api/register', 'POST');
INSERT INTO `sys_menu_perm` VALUES (250, 4, '/api/login', 'POST');
INSERT INTO `sys_menu_perm` VALUES (251, 4, '/api/update/token', 'POST');
INSERT INTO `sys_menu_perm` VALUES (252, 4, '/api/user/one/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (253, 4, '/api/user/:id/role', 'GET');
INSERT INTO `sys_menu_perm` VALUES (254, 4, '/api/user/role/update', 'POST');
INSERT INTO `sys_menu_perm` VALUES (255, 4, '/api/user', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (256, 4, '/api/user/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (257, 4, '/api/user/password/reset/:userId', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (258, 4, '/api/user/import', 'POST');
INSERT INTO `sys_menu_perm` VALUES (259, 4, '/api/perm/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (260, 4, '/api/perm/user', 'GET');
INSERT INTO `sys_menu_perm` VALUES (261, 4, '/api/perm/menu', 'GET');
INSERT INTO `sys_menu_perm` VALUES (262, 4, '/api/menu/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (263, 4, '/api/menu/one/:parentId/btns', 'GET');
INSERT INTO `sys_menu_perm` VALUES (264, 4, '/api/menu/one/:id/menu-perm', 'GET');
INSERT INTO `sys_menu_perm` VALUES (265, 4, '/api/menu', 'POST');
INSERT INTO `sys_menu_perm` VALUES (266, 4, '/api/menu', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (267, 4, '/api/menu/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (304, 3, '/api/user/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (305, 3, '/api/user/one/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (306, 3, '/api/update/token', 'POST');
INSERT INTO `sys_menu_perm` VALUES (307, 3, '/api/login', 'POST');
INSERT INTO `sys_menu_perm` VALUES (308, 3, '/api/register', 'POST');
INSERT INTO `sys_menu_perm` VALUES (309, 3, '/api/user/:id/role', 'GET');
INSERT INTO `sys_menu_perm` VALUES (310, 3, '/api/user/role/update', 'POST');
INSERT INTO `sys_menu_perm` VALUES (311, 3, '/api/user', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (312, 3, '/api/user/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (313, 3, '/api/user/password/reset/:userId', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (314, 3, '/api/user/import', 'POST');
INSERT INTO `sys_menu_perm` VALUES (315, 3, '/api/perm/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (316, 3, '/api/perm/user', 'GET');
INSERT INTO `sys_menu_perm` VALUES (317, 3, '/api/perm/menu', 'GET');
INSERT INTO `sys_menu_perm` VALUES (318, 3, '/api/menu/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (319, 3, '/api/menu/one/:parentId/btns', 'GET');
INSERT INTO `sys_menu_perm` VALUES (320, 3, '/api/menu/one/:id/menu-perm', 'GET');
INSERT INTO `sys_menu_perm` VALUES (321, 3, '/api/menu', 'POST');
INSERT INTO `sys_menu_perm` VALUES (322, 3, '/api/menu', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (323, 3, '/api/menu/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (324, 3, '/api/role/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (325, 3, '/api/role/one/:id/perms', 'GET');
INSERT INTO `sys_menu_perm` VALUES (326, 3, '/api/role', 'POST');
INSERT INTO `sys_menu_perm` VALUES (327, 3, '/api/role', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (328, 3, '/api/role/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (329, 3, '/api/dept', 'POST');
INSERT INTO `sys_menu_perm` VALUES (330, 3, '/api/dept', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (331, 3, '/api/dept/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (332, 3, '/api/dept/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (333, 3, '/api/oss/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (334, 3, '/api/oss/upload', 'POST');
INSERT INTO `sys_menu_perm` VALUES (335, 3, '/api/post/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (336, 3, '/api/post/:id', 'GET');
INSERT INTO `sys_menu_perm` VALUES (337, 3, '/api/post/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (338, 3, '/api/post', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (339, 3, '/api/post', 'POST');
INSERT INTO `sys_menu_perm` VALUES (379, 5, '/api/register', 'POST');
INSERT INTO `sys_menu_perm` VALUES (380, 5, '/api/login', 'POST');
INSERT INTO `sys_menu_perm` VALUES (381, 5, '/api/update/token', 'POST');
INSERT INTO `sys_menu_perm` VALUES (382, 5, '/api/user/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (383, 5, '/api/user/one/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (384, 5, '/api/user/:id/role', 'GET');
INSERT INTO `sys_menu_perm` VALUES (385, 5, '/api/user/role/update', 'POST');
INSERT INTO `sys_menu_perm` VALUES (386, 5, '/api/user', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (387, 5, '/api/user/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (388, 5, '/api/user/password/reset/:userId', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (389, 5, '/api/user/import', 'POST');
INSERT INTO `sys_menu_perm` VALUES (390, 5, '/api/perm/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (391, 5, '/api/perm/user', 'GET');
INSERT INTO `sys_menu_perm` VALUES (392, 5, '/api/perm/menu', 'GET');
INSERT INTO `sys_menu_perm` VALUES (393, 5, '/api/menu/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (394, 5, '/api/menu/one/:parentId/btns', 'GET');
INSERT INTO `sys_menu_perm` VALUES (395, 5, '/api/menu/one/:id/menu-perm', 'GET');
INSERT INTO `sys_menu_perm` VALUES (396, 5, '/api/menu', 'POST');
INSERT INTO `sys_menu_perm` VALUES (397, 5, '/api/menu', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (398, 5, '/api/menu/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (399, 5, '/api/role/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (400, 5, '/api/role/one/:id/perms', 'GET');
INSERT INTO `sys_menu_perm` VALUES (401, 5, '/api/role', 'POST');
INSERT INTO `sys_menu_perm` VALUES (402, 5, '/api/role', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (403, 5, '/api/role/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (404, 5, '/api/dept', 'POST');
INSERT INTO `sys_menu_perm` VALUES (405, 5, '/api/dept', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (406, 5, '/api/dept/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (407, 5, '/api/dept/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (408, 5, '/api/post', 'POST');
INSERT INTO `sys_menu_perm` VALUES (409, 5, '/api/post', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (410, 5, '/api/post/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (411, 5, '/api/post/:id', 'GET');
INSERT INTO `sys_menu_perm` VALUES (412, 5, '/api/post/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (413, 5, '/api/oss/upload', 'POST');
INSERT INTO `sys_menu_perm` VALUES (414, 5, '/api/oss/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (415, 33, '/api/user', 'POST');
INSERT INTO `sys_menu_perm` VALUES (420, 2, '/api/update/token', 'POST');
INSERT INTO `sys_menu_perm` VALUES (421, 2, '/api/login', 'POST');
INSERT INTO `sys_menu_perm` VALUES (422, 2, '/api/user/one/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (423, 2, '/api/register', 'POST');
INSERT INTO `sys_menu_perm` VALUES (424, 2, '/api/user/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (425, 2, '/api/user/:id/role', 'GET');
INSERT INTO `sys_menu_perm` VALUES (426, 2, '/api/user/role/update', 'POST');
INSERT INTO `sys_menu_perm` VALUES (427, 2, '/api/user', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (428, 2, '/api/user/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (429, 2, '/api/user/password/reset/:userId', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (430, 2, '/api/user/import', 'POST');
INSERT INTO `sys_menu_perm` VALUES (431, 2, '/api/perm/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (432, 2, '/api/perm/user', 'GET');
INSERT INTO `sys_menu_perm` VALUES (433, 2, '/api/perm/menu', 'GET');
INSERT INTO `sys_menu_perm` VALUES (434, 2, '/api/menu/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (435, 2, '/api/menu/one/:parentId/btns', 'GET');
INSERT INTO `sys_menu_perm` VALUES (436, 2, '/api/menu/one/:id/menu-perm', 'GET');
INSERT INTO `sys_menu_perm` VALUES (437, 2, '/api/menu', 'POST');
INSERT INTO `sys_menu_perm` VALUES (438, 2, '/api/menu', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (439, 2, '/api/menu/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (440, 2, '/api/role/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (441, 2, '/api/role/one/:id/perms', 'GET');
INSERT INTO `sys_menu_perm` VALUES (442, 2, '/api/role', 'POST');
INSERT INTO `sys_menu_perm` VALUES (443, 2, '/api/role', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (444, 2, '/api/role/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (445, 2, '/api/dept', 'POST');
INSERT INTO `sys_menu_perm` VALUES (446, 2, '/api/dept', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (447, 2, '/api/dept/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (448, 2, '/api/dept/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (449, 2, '/api/post', 'POST');
INSERT INTO `sys_menu_perm` VALUES (450, 2, '/api/post', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (451, 2, '/api/post/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (452, 2, '/api/post/:id', 'GET');
INSERT INTO `sys_menu_perm` VALUES (453, 2, '/api/post/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (454, 2, '/api/oss/upload', 'POST');
INSERT INTO `sys_menu_perm` VALUES (455, 2, '/api/oss/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (464, 36, '/api/partner', 'POST');
INSERT INTO `sys_menu_perm` VALUES (465, 37, '/api/partner', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (466, 38, '/api/partner', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (468, 39, '/api/partner/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (471, 41, '/api/info', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (472, 43, '/api/message/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (473, 44, '/api/message/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (520, 40, '/api/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (521, 40, '/api/info', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (522, 6, '/api/menu/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (523, 6, '/api/menu/one/:parentId/btns', 'GET');
INSERT INTO `sys_menu_perm` VALUES (524, 6, '/api/menu/one/:id/menu-perm', 'GET');
INSERT INTO `sys_menu_perm` VALUES (525, 6, '/api/register', 'POST');
INSERT INTO `sys_menu_perm` VALUES (526, 6, '/api/login', 'POST');
INSERT INTO `sys_menu_perm` VALUES (527, 6, '/api/update/token', 'POST');
INSERT INTO `sys_menu_perm` VALUES (528, 6, '/api/user/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (529, 6, '/api/user/one/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (530, 6, '/api/user/:id/role', 'GET');
INSERT INTO `sys_menu_perm` VALUES (531, 6, '/api/user/role/update', 'POST');
INSERT INTO `sys_menu_perm` VALUES (532, 6, '/api/user', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (533, 6, '/api/user/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (534, 6, '/api/user/password/reset/:userId', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (535, 6, '/api/user/import', 'POST');
INSERT INTO `sys_menu_perm` VALUES (536, 6, '/api/perm/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (537, 6, '/api/perm/user', 'GET');
INSERT INTO `sys_menu_perm` VALUES (538, 6, '/api/perm/menu', 'GET');
INSERT INTO `sys_menu_perm` VALUES (539, 6, '/api/menu', 'POST');
INSERT INTO `sys_menu_perm` VALUES (540, 6, '/api/menu', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (541, 6, '/api/menu/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (542, 6, '/api/role/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (543, 6, '/api/role/one/:id/perms', 'GET');
INSERT INTO `sys_menu_perm` VALUES (544, 6, '/api/role', 'POST');
INSERT INTO `sys_menu_perm` VALUES (545, 6, '/api/role', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (546, 6, '/api/role/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (547, 6, '/api/dept', 'POST');
INSERT INTO `sys_menu_perm` VALUES (548, 6, '/api/dept', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (549, 6, '/api/dept/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (550, 6, '/api/dept/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (551, 6, '/api/post', 'POST');
INSERT INTO `sys_menu_perm` VALUES (552, 6, '/api/post', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (553, 6, '/api/post/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (554, 6, '/api/post/:id', 'GET');
INSERT INTO `sys_menu_perm` VALUES (555, 6, '/api/post/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (556, 6, '/api/oss/upload', 'POST');
INSERT INTO `sys_menu_perm` VALUES (557, 6, '/api/oss/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (558, 7, '/api/oss/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (559, 7, '/api/register', 'POST');
INSERT INTO `sys_menu_perm` VALUES (560, 7, '/api/login', 'POST');
INSERT INTO `sys_menu_perm` VALUES (561, 7, '/api/update/token', 'POST');
INSERT INTO `sys_menu_perm` VALUES (562, 7, '/api/user/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (563, 7, '/api/user/one/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (564, 7, '/api/user/:id/role', 'GET');
INSERT INTO `sys_menu_perm` VALUES (565, 7, '/api/user/role/update', 'POST');
INSERT INTO `sys_menu_perm` VALUES (566, 7, '/api/user', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (567, 7, '/api/user/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (568, 7, '/api/user/password/reset/:userId', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (569, 7, '/api/user/import', 'POST');
INSERT INTO `sys_menu_perm` VALUES (570, 7, '/api/perm/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (571, 7, '/api/perm/user', 'GET');
INSERT INTO `sys_menu_perm` VALUES (572, 7, '/api/menu/all', 'GET');
INSERT INTO `sys_menu_perm` VALUES (573, 7, '/api/perm/menu', 'GET');
INSERT INTO `sys_menu_perm` VALUES (574, 7, '/api/menu/one/:parentId/btns', 'GET');
INSERT INTO `sys_menu_perm` VALUES (575, 7, '/api/menu/one/:id/menu-perm', 'GET');
INSERT INTO `sys_menu_perm` VALUES (576, 7, '/api/menu', 'POST');
INSERT INTO `sys_menu_perm` VALUES (577, 7, '/api/menu', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (578, 7, '/api/menu/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (579, 7, '/api/role/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (580, 7, '/api/role/one/:id/perms', 'GET');
INSERT INTO `sys_menu_perm` VALUES (581, 7, '/api/role', 'POST');
INSERT INTO `sys_menu_perm` VALUES (582, 7, '/api/role', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (583, 7, '/api/role/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (584, 7, '/api/dept', 'POST');
INSERT INTO `sys_menu_perm` VALUES (585, 7, '/api/dept', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (586, 7, '/api/dept/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (587, 7, '/api/dept/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (588, 7, '/api/post', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (589, 7, '/api/post', 'POST');
INSERT INTO `sys_menu_perm` VALUES (590, 7, '/api/post/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (591, 7, '/api/post/:id', 'GET');
INSERT INTO `sys_menu_perm` VALUES (592, 7, '/api/post/:id', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (593, 7, '/api/oss/upload', 'POST');
INSERT INTO `sys_menu_perm` VALUES (594, 45, '/api/operalog', 'POST');
INSERT INTO `sys_menu_perm` VALUES (595, 45, '/api/operalog/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (596, 46, '/api/loginlog', 'POST');
INSERT INTO `sys_menu_perm` VALUES (597, 46, '/api/loginlog/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (605, 48, '/api/dict', 'POST');
INSERT INTO `sys_menu_perm` VALUES (606, 49, '/api/dict', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (607, 50, '/api/dict/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (608, 51, '/api/dict/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (609, 47, '/api/dict/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (610, 47, '/api/dict', 'POST');
INSERT INTO `sys_menu_perm` VALUES (611, 47, '/api/dict', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (612, 47, '/api/dict/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (613, 47, '/api/dict/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (614, 47, '/api/dict/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (615, 47, '/api/dict/list/data', 'GET');
INSERT INTO `sys_menu_perm` VALUES (616, 47, '/api/dict/data', 'POST');
INSERT INTO `sys_menu_perm` VALUES (617, 47, '/api/dict/data', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (618, 47, '/api/dict/data/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (619, 47, '/api/dict/data/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (620, 47, '/api/dict/data/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (621, 52, '/api/dict/data/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (622, 53, '/api/dict/data/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (623, 54, '/api/dict/data', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (624, 55, '/api/dict/data', 'POST');
INSERT INTO `sys_menu_perm` VALUES (635, 57, '/api/banner', 'POST');
INSERT INTO `sys_menu_perm` VALUES (636, 58, '/api/banner', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (637, 59, '/api/banner/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (638, 60, '/api/banner/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (652, 62, '/api/homeContent', 'POST');
INSERT INTO `sys_menu_perm` VALUES (653, 63, '/api/homeContent', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (654, 64, '/api/homeContent/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (655, 65, '/api/homeContent/list/contentList', 'GET');
INSERT INTO `sys_menu_perm` VALUES (658, 67, '/api/setting', 'GET');
INSERT INTO `sys_menu_perm` VALUES (659, 67, '/api/setting', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (666, 70, '/api/content/tags', 'POST');
INSERT INTO `sys_menu_perm` VALUES (667, 72, '/api/content/tags/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (668, 71, '/api/content/tags', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (669, 73, '/api/content/tags/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (675, 75, '/api/content/column', 'POST');
INSERT INTO `sys_menu_perm` VALUES (676, 76, '/api/content/column', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (677, 77, '/api/content/column/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (684, 80, '/api/content', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (685, 79, '/api/content', 'POST');
INSERT INTO `sys_menu_perm` VALUES (686, 81, '/api/content/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (687, 82, '/api/content/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (690, 35, '/api/partner/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (691, 35, '/api/partner', 'POST');
INSERT INTO `sys_menu_perm` VALUES (692, 35, '/api/partner', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (693, 35, '/api/partner/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (694, 35, '/api/partner/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (695, 35, '/api/partner/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (711, 85, '/api/clientuser/web/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (712, 86, '/api/clientuser/status', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (713, 87, '/api/access/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (714, 87, '/api/access/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (716, 89, '/api/access/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (717, 84, '/api/clientuser/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (718, 84, '/api/clientuser/status', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (719, 84, '/api/clientuser/web/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (720, 42, '/api/message/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (721, 42, '/api/message/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (722, 42, '/api/message/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (723, 42, '/api/message/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (724, 90, '/api/content/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (737, 69, '/api/content/tags/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (738, 69, '/api/content/tags', 'POST');
INSERT INTO `sys_menu_perm` VALUES (739, 69, '/api/content/tags', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (740, 69, '/api/content/tags/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (741, 69, '/api/content/tags/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (742, 69, '/api/content/tags/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (748, 61, '/api/homeContent', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (749, 61, '/api/homeContent/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (750, 61, '/api/homeContent/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (751, 61, '/api/homeContent', 'POST');
INSERT INTO `sys_menu_perm` VALUES (752, 61, '/api/homeContent/info/:code', 'GET');
INSERT INTO `sys_menu_perm` VALUES (753, 61, '/api/homeContent/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (754, 61, '/api/homeContent/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (755, 61, '/api/homeContent/list/contentList', 'GET');
INSERT INTO `sys_menu_perm` VALUES (756, 61, '/api/homeContent/contentList', 'POST');
INSERT INTO `sys_menu_perm` VALUES (757, 61, '/api/homeContent/contentList', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (758, 61, '/api/homeContent/contentList/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (759, 61, '/api/homeContent/contentList/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (760, 61, '/api/homeContent/contentList/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (765, 94, '/api/projectMonitoring/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (766, 94, '/api/projectMonitoring', 'POST');
INSERT INTO `sys_menu_perm` VALUES (767, 94, '/api/projectMonitoring', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (768, 94, '/api/projectMonitoring/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (769, 94, '/api/projectMonitoring/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (770, 94, '/api/projectMonitoring/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (771, 95, '/api/projectMonitoring', 'POST');
INSERT INTO `sys_menu_perm` VALUES (772, 96, '/api/projectMonitoring', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (773, 97, '/api/projectMonitoring/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (774, 98, '/api/projectMonitoring/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (775, 56, '/api/banner/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (776, 56, '/api/banner', 'POST');
INSERT INTO `sys_menu_perm` VALUES (777, 56, '/api/banner', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (778, 56, '/api/banner/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (779, 56, '/api/banner/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (780, 56, '/api/banner/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (781, 34, '/api/login', 'POST');
INSERT INTO `sys_menu_perm` VALUES (782, 34, '/api/loginOut', 'GET');
INSERT INTO `sys_menu_perm` VALUES (783, 34, '/api/update/token', 'POST');
INSERT INTO `sys_menu_perm` VALUES (784, 34, '/api/oss/upload', 'POST');
INSERT INTO `sys_menu_perm` VALUES (785, 74, '/api/content/column/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (786, 74, '/api/content/column', 'POST');
INSERT INTO `sys_menu_perm` VALUES (787, 74, '/api/content/column', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (788, 74, '/api/content/column/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (789, 74, '/api/content/column/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (790, 74, '/api/content/column/AllList', 'GET');
INSERT INTO `sys_menu_perm` VALUES (791, 78, '/api/content/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (792, 78, '/api/content', 'POST');
INSERT INTO `sys_menu_perm` VALUES (793, 78, '/api/content', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (794, 78, '/api/content/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (795, 78, '/api/content/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (796, 78, '/api/content/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (797, 78, '/api/content/tags/AllList', 'GET');
INSERT INTO `sys_menu_perm` VALUES (808, 102, '/api/customer/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (809, 102, '/api/customer', 'POST');
INSERT INTO `sys_menu_perm` VALUES (810, 102, '/api/customer', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (811, 102, '/api/customer/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (812, 102, '/api/customer/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (813, 101, '/api/customer', 'POST');
INSERT INTO `sys_menu_perm` VALUES (814, 101, '/api/customer', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (815, 101, '/api/customer/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (816, 101, '/api/customer/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (817, 101, '/api/customer/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (818, 103, '/api/customer', 'POST');
INSERT INTO `sys_menu_perm` VALUES (819, 104, '/api/customer', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (820, 105, '/api/customer/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (826, 106, '/api/supplier/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (827, 106, '/api/supplier', 'POST');
INSERT INTO `sys_menu_perm` VALUES (828, 106, '/api/supplier', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (829, 106, '/api/supplier/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (830, 106, '/api/supplier/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (831, 107, '/api/supplier', 'POST');
INSERT INTO `sys_menu_perm` VALUES (832, 108, '/api/supplier', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (833, 109, '/api/supplier/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (834, 110, '/api/equipment/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (835, 110, '/api/equipment', 'POST');
INSERT INTO `sys_menu_perm` VALUES (836, 110, '/api/equipment', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (837, 110, '/api/equipment/info', 'GET');
INSERT INTO `sys_menu_perm` VALUES (838, 110, '/api/equipment/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (839, 111, '/api/equipment', 'POST');
INSERT INTO `sys_menu_perm` VALUES (840, 112, '/api/equipment', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (841, 113, '/api/equipment/delete', 'DELETE');
INSERT INTO `sys_menu_perm` VALUES (842, 114, '/api/equipment/status/change', 'PUT');
INSERT INTO `sys_menu_perm` VALUES (843, 115, '/api/public/messageNumber', 'GET');
INSERT INTO `sys_menu_perm` VALUES (844, 116, '/api/public/getAccess', 'GET');
INSERT INTO `sys_menu_perm` VALUES (845, 117, '/api/public/access/statis', 'GET');
INSERT INTO `sys_menu_perm` VALUES (846, 118, '/api/public/clientUserList', 'GET');
INSERT INTO `sys_menu_perm` VALUES (847, 99, '/api/projectMonitoring/log/list', 'GET');
INSERT INTO `sys_menu_perm` VALUES (848, 99, '/api/projectMonitoring/logOne', 'GET');

-- ----------------------------
-- Table structure for sys_operalog
-- ----------------------------
DROP TABLE IF EXISTS `sys_operalog`;
CREATE TABLE `sys_operalog`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `system_menu` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统菜单',
  `opera_module` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '操作模块',
  `opera_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '操作人员',
  `opera_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '操作IP',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-成功，0-失败',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件 url',
  `size` int NOT NULL COMMENT '文件size',
  `location` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件存放位置',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `business` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '业务描述字段，可以字符串，也可以是 JSON 字符串',
  `user_id` bigint NOT NULL COMMENT '上传用户id',
  `user_account` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '上传用户帐号',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件mimetype类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 232 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oss
-- ----------------------------
INSERT INTO `sys_oss` VALUES (220, '/static/0d7d7348372d4e959d0429bbf52ea42a.jpeg', 296974, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\0d7d7348372d4e959d0429bbf52ea42a.jpeg', '2024-01-29 13:57:39.129858', 'Banner-移动端图片', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (221, '/static/fb821e7668524e48a0aaeb9711762115.jpeg', 105461, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\fb821e7668524e48a0aaeb9711762115.jpeg', '2024-01-29 13:57:45.464638', 'Banner-移动端图片', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (222, '/static/64be6560755c4ff4b00712213de5b263.jpeg', 185028, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\64be6560755c4ff4b00712213de5b263.jpeg', '2024-01-29 14:14:35.041238', '首页内容', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (223, '/static/697d9a18331b40069bab5fec52a7592d.jpeg', 185028, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\697d9a18331b40069bab5fec52a7592d.jpeg', '2024-01-29 14:19:11.345213', '首页内容', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (224, '/static/3184281db5c043e8aa237c6663d2abb7.jpeg', 185028, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\3184281db5c043e8aa237c6663d2abb7.jpeg', '2024-01-29 14:20:24.013269', '首页内容', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (225, '/static/14412c4a44e84f0ab3b9e1a54a7d5af5.jpeg', 178937, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\14412c4a44e84f0ab3b9e1a54a7d5af5.jpeg', '2024-01-29 14:23:53.437187', '首页内容', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (226, '/static/f839d46a9b88460d96c4b89001c7b2ab.jpeg', 178937, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\f839d46a9b88460d96c4b89001c7b2ab.jpeg', '2024-01-29 14:25:56.442206', '首页内容', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (227, '/static/f40ff67776be41abba53dbac7b83e3b5.jpeg', 66958, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\f40ff67776be41abba53dbac7b83e3b5.jpeg', '2024-01-29 14:27:54.074781', '首页内容', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (228, '/static/b825505fd8014cd4b8a0db894c56b235.jpeg', 178937, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\b825505fd8014cd4b8a0db894c56b235.jpeg', '2024-01-29 14:31:39.191497', '首页内容', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (229, '/static/d3f59ec76d1242569a8ce9db75feaea5.jpeg', 66958, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\d3f59ec76d1242569a8ce9db75feaea5.jpeg', '2024-01-29 14:33:13.568618', '首页内容', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (230, '/static/ea34a24f6ffa412f9724bdd114770b62.jpeg', 66958, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\ea34a24f6ffa412f9724bdd114770b62.jpeg', '2024-01-29 14:34:18.594589', '首页内容', 1, 'admin', 'image/jpeg');
INSERT INTO `sys_oss` VALUES (231, '/static/0c80af098a3441eba79ab49b52f825bd.jpeg', 66958, 'E:\\project\\nestJs\\dashan-nest-admin\\upload\\0c80af098a3441eba79ab49b52f825bd.jpeg', '2024-01-29 14:37:39.579792', '首页内容', 1, 'admin', 'image/jpeg');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '岗位编码',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '岗位状态，1-有效，0-禁用',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '备注',
  `order_num` int NOT NULL DEFAULT 0 COMMENT '排序',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '岗位名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '角色备注',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (5, '演示角色', '演示角色', '2024-02-19 09:11:31.069267', '2024-03-19 11:30:08.000000');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `role_id` bigint NOT NULL COMMENT '角色 id',
  `menu_id` bigint NOT NULL COMMENT '菜单 id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (302, 5, 1);
INSERT INTO `sys_role_menu` VALUES (303, 5, 115);
INSERT INTO `sys_role_menu` VALUES (304, 5, 116);
INSERT INTO `sys_role_menu` VALUES (305, 5, 117);
INSERT INTO `sys_role_menu` VALUES (306, 5, 118);
INSERT INTO `sys_role_menu` VALUES (307, 5, 3);
INSERT INTO `sys_role_menu` VALUES (308, 5, 34);
INSERT INTO `sys_role_menu` VALUES (309, 5, 56);
INSERT INTO `sys_role_menu` VALUES (310, 5, 57);
INSERT INTO `sys_role_menu` VALUES (311, 5, 58);
INSERT INTO `sys_role_menu` VALUES (312, 5, 59);
INSERT INTO `sys_role_menu` VALUES (313, 5, 60);
INSERT INTO `sys_role_menu` VALUES (314, 5, 61);
INSERT INTO `sys_role_menu` VALUES (315, 5, 62);
INSERT INTO `sys_role_menu` VALUES (316, 5, 63);
INSERT INTO `sys_role_menu` VALUES (317, 5, 64);
INSERT INTO `sys_role_menu` VALUES (318, 5, 65);
INSERT INTO `sys_role_menu` VALUES (319, 5, 66);
INSERT INTO `sys_role_menu` VALUES (320, 5, 68);
INSERT INTO `sys_role_menu` VALUES (321, 5, 69);
INSERT INTO `sys_role_menu` VALUES (322, 5, 70);
INSERT INTO `sys_role_menu` VALUES (323, 5, 71);
INSERT INTO `sys_role_menu` VALUES (324, 5, 72);
INSERT INTO `sys_role_menu` VALUES (325, 5, 73);
INSERT INTO `sys_role_menu` VALUES (326, 5, 92);
INSERT INTO `sys_role_menu` VALUES (327, 5, 74);
INSERT INTO `sys_role_menu` VALUES (328, 5, 75);
INSERT INTO `sys_role_menu` VALUES (329, 5, 76);
INSERT INTO `sys_role_menu` VALUES (330, 5, 77);
INSERT INTO `sys_role_menu` VALUES (331, 5, 78);
INSERT INTO `sys_role_menu` VALUES (332, 5, 79);
INSERT INTO `sys_role_menu` VALUES (333, 5, 80);
INSERT INTO `sys_role_menu` VALUES (334, 5, 81);
INSERT INTO `sys_role_menu` VALUES (335, 5, 82);
INSERT INTO `sys_role_menu` VALUES (336, 5, 90);
INSERT INTO `sys_role_menu` VALUES (337, 5, 83);
INSERT INTO `sys_role_menu` VALUES (338, 5, 35);
INSERT INTO `sys_role_menu` VALUES (339, 5, 36);
INSERT INTO `sys_role_menu` VALUES (340, 5, 37);
INSERT INTO `sys_role_menu` VALUES (341, 5, 38);
INSERT INTO `sys_role_menu` VALUES (342, 5, 39);
INSERT INTO `sys_role_menu` VALUES (343, 5, 42);
INSERT INTO `sys_role_menu` VALUES (344, 5, 43);
INSERT INTO `sys_role_menu` VALUES (345, 5, 44);
INSERT INTO `sys_role_menu` VALUES (346, 5, 84);
INSERT INTO `sys_role_menu` VALUES (347, 5, 85);
INSERT INTO `sys_role_menu` VALUES (348, 5, 86);
INSERT INTO `sys_role_menu` VALUES (349, 5, 91);
INSERT INTO `sys_role_menu` VALUES (350, 5, 87);
INSERT INTO `sys_role_menu` VALUES (351, 5, 89);
INSERT INTO `sys_role_menu` VALUES (352, 5, 93);
INSERT INTO `sys_role_menu` VALUES (353, 5, 94);
INSERT INTO `sys_role_menu` VALUES (354, 5, 95);
INSERT INTO `sys_role_menu` VALUES (355, 5, 96);
INSERT INTO `sys_role_menu` VALUES (356, 5, 97);
INSERT INTO `sys_role_menu` VALUES (357, 5, 98);
INSERT INTO `sys_role_menu` VALUES (358, 5, 99);
INSERT INTO `sys_role_menu` VALUES (359, 5, 101);
INSERT INTO `sys_role_menu` VALUES (360, 5, 102);
INSERT INTO `sys_role_menu` VALUES (361, 5, 103);
INSERT INTO `sys_role_menu` VALUES (362, 5, 104);
INSERT INTO `sys_role_menu` VALUES (363, 5, 105);
INSERT INTO `sys_role_menu` VALUES (364, 5, 106);
INSERT INTO `sys_role_menu` VALUES (365, 5, 107);
INSERT INTO `sys_role_menu` VALUES (366, 5, 108);
INSERT INTO `sys_role_menu` VALUES (367, 5, 109);
INSERT INTO `sys_role_menu` VALUES (368, 5, 110);
INSERT INTO `sys_role_menu` VALUES (369, 5, 111);
INSERT INTO `sys_role_menu` VALUES (370, 5, 112);
INSERT INTO `sys_role_menu` VALUES (371, 5, 113);
INSERT INTO `sys_role_menu` VALUES (372, 5, 114);

-- ----------------------------
-- Table structure for sys_setting
-- ----------------------------
DROP TABLE IF EXISTS `sys_setting`;
CREATE TABLE `sys_setting`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '邮箱授权码',
  `email_mail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '发送邮件的邮箱',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `sms_access_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '短信AccessKey',
  `sms_access_secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '短信Access密钥',
  `sms_sign_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '短信签名',
  `sms_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '短信模板code',
  `email_port` decimal(10, 0) NOT NULL DEFAULT 587 COMMENT '邮件端口465或587',
  `oss_access_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'OSS的AccessKey',
  `oss_access_secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'OSS的Access密钥',
  `oss_region` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'oss的region',
  `oss_bucket` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'oss的bucket',
  `receive_mail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '发送邮件的邮箱',
  `email_marketing` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '邮件营销语',
  `logistics_user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '快递鸟用户id',
  `logistics_key` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '快递鸟API的key',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户登录密码',
  `salt` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '盐',
  `account` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户登录账号',
  `phone_num` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户手机号码',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-有效，0-禁用',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '头像地址',
  `type` tinyint NOT NULL DEFAULT 1 COMMENT '帐号类型：0-超管， 1-普通用户',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, '$2a$10$JChCYKwJYbVV4ANalu2tBeamIJesb/nkT/uNL47CoYuIP7DlttqWK', '$2a$10$JChCYKwJYbVV4ANalu2tBe', 'admin', '13188877295', '375149069@qq.com', 1, '', 0, '2021-11-15 16:09:23.000000', '2024-03-19 13:43:15.000000');
INSERT INTO `sys_user` VALUES (8, '$2a$10$iE4xUOSqzLEsTNuOBR9nvuYo/7uHQ.pDVj1FjAMx.o.OOMcNxovMa', '$2a$10$iE4xUOSqzLEsTNuOBR9nvu', 'demoUser', '13166677295', '375149068@qq.com', 1, '', 1, '2024-02-19 09:16:51.000000', '2024-02-19 09:16:51.000000');

-- ----------------------------
-- Table structure for sys_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_dept`;
CREATE TABLE `sys_user_dept`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL COMMENT '用户id',
  `dept_id` bigint NOT NULL COMMENT '部门id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL COMMENT '用户id',
  `post_id` bigint NOT NULL COMMENT '岗位id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL COMMENT '用户id',
  `role_id` bigint NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (63, 8, 5);

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签名称',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '所属状态: 1-展示，0-隐藏',
  `is_delete` tinyint NOT NULL DEFAULT 1 COMMENT '所属删除逻辑: 1-不删除，0-删除',
  `order_num` int NOT NULL COMMENT '标签排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for web_info
-- ----------------------------
DROP TABLE IF EXISTS `web_info`;
CREATE TABLE `web_info`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '品牌名称' COMMENT '品牌名称',
  `company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '公司名称' COMMENT '公司名称',
  `wechat_public_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公众号名称',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '联系方式',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '地址',
  `copyright` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '版权',
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '网站logo',
  `qr_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '二维码',
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `web_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '网站名称' COMMENT '网站名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '网站描述' COMMENT '网站名称',
  `keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '网站关键词' COMMENT '网站名称',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱',
  `wechat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '官方微信号',
  `skype` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'skype',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of web_info
-- ----------------------------
INSERT INTO `web_info` VALUES (1, '官方网站', '官方网站', '官方网站', '13999888221', 'Longgang District, Shenzhen City, Guangdong Province, China', '粤ICP备', '/static/4a166b664cb540bab9a8772086be2d0b.jpeg', '/static/a7e9b1614a1e42b8994df5b0a978ae14.png', '2023-10-10 10:24:52.261115', '2024-02-19 10:01:31.000000', '官方网站', '官方网站官方网站', '官方网站', '2222@qq.com', 'xxxx', 'skype 号');

SET FOREIGN_KEY_CHECKS = 1;
